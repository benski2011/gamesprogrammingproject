#include "AStar.h"

Map* AStar::map = nullptr;

std::vector<glm::vec2> AStar::aStar(glm::vec2 & startPos, glm::vec2 & endPosition)
{
	float cost = 10.0f;

	struct AStarTile
	{
		int x;
		int y;

		int g = 10000;
		int h;
		int f;
		bool closed = false;
		int parentIndex = -1;
	};

	std::vector<AStarTile> open;
	std::vector<glm::vec2> pathHome;

	sf::Vector2i endPos(endPosition.x / map->size, endPosition.y / map->size);

	AStarTile currentTile;
	currentTile.x = startPos.x / map->size;
	currentTile.y = startPos.y / map->size;

	if (endPos.x < 0 || endPos.y < 0 ||
		endPos.x >= map->tiles.size() || endPos.y >= map->tiles[0].size()
		|| currentTile.x < 0 || currentTile.y < 0
		|| currentTile.x >= map->tiles.size() || currentTile.y >= map->tiles[0].size())
		//|| map->tiles[endPos.x][endPos.y].ter == UNWALKABLE)
	{
		return pathHome;
	}

	currentTile.g = 0;
	currentTile.h = manhattanDistance(sf::Vector2i(currentTile.x, currentTile.y), endPos) * cost;
	currentTile.f = currentTile.h + currentTile.g;

	open.push_back(currentTile);

	AStarTile neighbour;

	while (true)
	{
		currentTile.f = 100000;
		int parent = -1;

		for (int i = 0; i < open.size(); ++i)
		{
			if (currentTile.f >= open[i].f &&
				!open[i].closed)
			{
				currentTile = open[i];
				parent = i;
			}
		}
		if ((currentTile.x == endPos.x &&
			currentTile.y == endPos.y))
		{
			break;
		}

		open[parent].closed = true;

		for (int i = -1; i < 2; ++i)
		{
			for (int j = -1; j < 2; ++j)
			{
				if (abs(i) == abs(j))
				{
					continue;
				}
				if (currentTile.x + i >= 0
					&& currentTile.y + j >= 0
					&& currentTile.x + i < map->tiles.size()
					&& currentTile.y + j < map->tiles[0].size())
				{
					bool found = false;

					int newG;
					//if (map->tiles[currentTile.x + i][currentTile.y + j].ter == UNWALKABLE)
					//{
					//	newG = 2000;
					//}
					//else
					{
						newG = currentTile.g + (cost * (10.0f - map->tiles[currentTile.x + i][currentTile.y + j].walkSpeed));
					}

					for (int k = 0; k < open.size(); ++k)
					{
						if (open[k].x == currentTile.x + i &&
							open[k].y == currentTile.y + j)
						{
							found = true;
							if (!open[k].closed)
							{
								if (open[k].g > newG)
								{
									open[k].g = newG;
									open[k].f = newG + open[k].h;
									open[k].parentIndex = parent;
								}
							}
						}
					}

					if (!found)
					{
						neighbour.x = currentTile.x + i;
						neighbour.y = currentTile.y + j;

						//if (map->tiles[currentTile.x + i][currentTile.y + j].ter == UNWALKABLE)
						//{
						//	neighbour.g = 2000;
						//}
						//else
						{
							neighbour.g = currentTile.g + (cost * (10.0f - map->tiles[currentTile.x + i][currentTile.y + j].walkSpeed));
						}
						neighbour.h = manhattanDistance(sf::Vector2i(neighbour.x, neighbour.y), endPos) * cost;
						neighbour.f = neighbour.g + neighbour.h;

						neighbour.parentIndex = parent;

						open.push_back(neighbour);
					}
				}
			}
		}
	}

	if (currentTile.f < 2000)
	{
		while (currentTile.parentIndex != -1)
		{
			pathHome.push_back(glm::vec2(currentTile.x * map->size + map->size / 2, currentTile.y * map->size + map->size / 2));
			currentTile = open[currentTile.parentIndex];
		}
		std::reverse(pathHome.begin(), pathHome.end());
	}

	return pathHome;
}

int AStar::manhattanDistance(sf::Vector2i & pos, sf::Vector2i& goal)
{
	return abs(pos.x - goal.x) + abs(pos.y - goal.y);
}

void AStar::setMap(Map * mapPointer)
{
	map = mapPointer;
}