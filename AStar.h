#pragma once

#include <SFML\Graphics\Vertex.hpp>
#include <vector>

#include "Map.h"

class Map;

class AStar
{
public:
	/**
	* Uses AStar to create a path between 2 points.
	*
	* @param startPos Start position
	* @param endPos End position
	*/
	static std::vector<glm::vec2> aStar(glm::vec2& startPos, glm::vec2& endPos);

	/**
	* Calculates the manhatten distance between 2 points
	*
	* @param pos First point
	* @param goal Second point
	*/
	static int manhattanDistance(sf::Vector2i & pos, sf::Vector2i& goal);

	/**
	* Sets the map used for AStar.
	*
	* @param map The new map
	*/
	static void setMap(Map* map);

private:
	AStar();

	static Map* map;
};
