#pragma once

#include "Resources.h"
#include "ResearchHandler.h"

/**
*	The resources and amount of said resources
*/
struct ResWares
{
	Resources::Resource resItem;
	int amount;
};

/**
*	The tradegoods and amount of said resources
*/
struct TradeWares
{
	Resources::TradeGoods tradeItem;
	int amount;
};

/**
*	A set of reswares and tradewares
*/
struct ItemSet
{
	std::vector<ResWares> resWares;
	std::vector<TradeWares> tradeWares;
};

struct BuildingLevel
{
	std::string name;
	std::string description;
	ItemSet outPut;
	ItemSet price;
	ItemSet consumed;
	std::string model;
	std::vector<Research*> required;
	int buildingTime;
	int maxPawns;
};
