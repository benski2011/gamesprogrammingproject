#pragma once

#include "Building.h"
#include <fstream>
#include <vector>

struct building
{
	/*
	*	A specific building holds what type of building it is, where it exists and which entities are inside of it
	**/
	Building type;
	sf::Vector2i position;
	std::vector<size_t> entities;
	unsigned int allowedEntities = 0;
};

class Buildings
{
public:
	std::vector<Building> buildingTypes;
	std::vector<building> buildings;
	Buildings() {};
	/*
	*	The constructor for building types
	*	Loads in all the buildings from a file using the format specified within the function
	*	@param the existing resources and tradegoods and relevant functions
	*	@param the existing researches and get from name functions relevant to them
	**/
	Buildings(Resources* resources, ResearchHandler* research);
	~Buildings();
};
