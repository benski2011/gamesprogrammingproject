#pragma once

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <GL\glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera
{
public:
	/**
	* Constucts a empty camera
	*/
	Camera() {};

	/**
	* Constructs a camera with the given resolution, position and front vector
	*
	* @param resolution The resolution of the camera
	* @param position The position of the camera
	* @param front The front vector of the camera
	*/
	Camera(float resolution, glm::vec3 position, glm::vec3 front);
	~Camera();

	/**
	* Moves the camera.
	*
	* @param offset Position offset to move the camera with
	*/
	void move(glm::vec3 offset);

	/**
	* Sets the viewMatrix for the camera
	* @param position 	The new position of the camera
	* @param front  	Direction to look
	*/
	void lookAt(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 front = glm::vec3(0.0f, 0.0f, 0.0f));

	/**
	* Sets the perspective matrix for the camera
	* @param near Near clip plane
	* @param far  Far clip plane
	* @param fov  Field of view angle in radians
	*/
	void setProjection(float cameraNear, float cameraFar, float resolution, float fov = glm::half_pi<float>());

	/**
	* Bind the view and projection matrix for the given shader program
	*
	* @param shaderProgram The shader program
	*/
	void bindUniforms(GLuint shaderProgram);

	/**
	* Gets the position of the camera
	*
	* @return The position of the camera
	*/
	glm::vec3 getPos();
private:
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec3 position;
	glm::vec3 front = glm::vec3(1.0f, -1.0f, 0.0f);
};