#include "CityBuildingGui.h"

void CityBuildingGui::citySetUp(EventQueue<GUIEvent>* eventQueue, std::vector<Building>* buildings, Resources* resources)
{
	this->buildings = buildings;
	this->resources = resources;
	counter = 0;
	page = 0;
	mainBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	buildTableBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	menu = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	buildingButton = sfg::Button::Create("Building");
	buildingButton->GetSignal(sfg::Widget::OnMouseLeftPress).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::ACTIVATEBUILDINGMENU;
		event.active.active = true;
		eventQueue->pushEvent(event); });

	menu->Pack(buildingButton, false, false);
	mainBox->Pack(menu, false, false);

	buildTable = sfg::Table::Create();
	for (size_t i = 0; i < buildings->size(); i++)
	{
		addBuildingButton(eventQueue, buildings->at(i).levels[0].name);
	}

	buildTableBox->Pack(buildTable, true, true);
	mainBox->Pack(buildTableBox);

	auto up = sfg::Button::Create("UP");
	up->GetSignal(sfg::Widget::OnMouseLeftPress).Connect([this] { moveBuildList(Direction::UP); });
	auto down = sfg::Button::Create("Down");
	down->GetSignal(sfg::Widget::OnMouseLeftPress).Connect([this] { moveBuildList(Direction::DOWN); });
	auto BackBuildingButton = sfg::Button::Create("Back");
	BackBuildingButton->GetSignal(sfg::Widget::OnMouseLeftPress).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::ACTIVATEBUILDINGMENU;
		event.active.active = false;
		eventQueue->pushEvent(event); });

	buildTable->Attach(up, sf::Rect<sf::Uint32>(0, BuildingHandlerPerShow / 2, 1, 1));
	buildTable->Attach(down, sf::Rect<sf::Uint32>(1, BuildingHandlerPerShow / 2, 1, 1));
	buildTable->Attach(BackBuildingButton, sf::Rect<sf::Uint32>(0, BuildingHandlerPerShow / 2 + 1, 2, 1));
	buildTable->SetAllocation(sf::FloatRect(0, 300, 200, 300));
	buildTableBox->Show(false);

	buildingButton->SetRequisition(sf::Vector2f(buildTable->GetRequisition().x, 100));

	infoWindow = sfg::Window::Create();
	infoWindow->SetStyle(infoWindow->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	infoWindow->SetAllocation(sf::FloatRect(buildTable->GetRequisition().x + 150, 600, 900, 250));
	infoWindow->SetRequisition(sf::Vector2f(900, 300));
	auto mainInfoBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	infoWindow->Add(mainInfoBox);

	//this is the main box for buildings info screen
	buildingBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	mainInfoBox->Pack(buildingBox);
	buildingBox->Show(false);

	buildingFrame = sfg::Frame::Create("name of thing here");
	buildingBox->Pack(buildingFrame);
	auto frameBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	BuildingInfoLabel = sfg::Label::Create("This is where the information text will be\n a Combo of flavor text and usefull info here\n this is the fill in more then is requred");
	//BuildingInfoLabel->SetRequisition(sf::Vector2f(0, 200));
	frameBox->Pack(BuildingInfoLabel, false, false);
	buildingFrame->Add(frameBox);
	underBox = sfg::Box::Create();
	buildingBox->Pack(underBox, false, false);
	auto costFrame = sfg::Frame::Create("Cost: ");
	costFrame->SetRequisition(sf::Vector2f(150, 0));
	underBox->Pack(costFrame, false, false);
	auto newBox2 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	costFrame->Add(newBox2);

	auto outputFrame = sfg::Frame::Create("Output: ");
	outputFrame->SetRequisition(sf::Vector2f(300, 0));
	underBox->Pack(outputFrame, false, false);
	auto newBox3 = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);

	auto PawnFrame = sfg::Frame::Create("Villagers: ");
	PawnFrame->SetRequisition(sf::Vector2f(150, 0));
	underBox->Pack(PawnFrame, false, false);
	PawnFrame->Add(newBox3);
	auto newBox4 = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	outputFrame->Add(newBox4);
	auto newBox5 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	newBox4->Pack(newBox5);
	auto newBox6 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	newBox4->Pack(newBox6);
	pawn = sfg::Label::Create("Villager: 3");
	newBox3->Pack(pawn);

	for (size_t i = 0; i < 3; i++)
	{
		auto outputTemp = sfg::Label::Create("Wood: 20");
		newBox5->Pack(outputTemp);
		output.push_back(outputTemp);
	}
	for (size_t i = 0; i < 3; i++)
	{
		auto outputTemp = sfg::Label::Create("Wood: 20");
		newBox6->Pack(outputTemp);
		output.push_back(outputTemp);
	}

	for (size_t i = 0; i < 3; i++)
	{
		auto costTemp = sfg::Label::Create("Wood: 150");
		newBox2->Pack(costTemp);
		cost.push_back(costTemp);
	}

	buildingPointBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	setUpClickBuilding(buildingPointBox, eventQueue);
	buildingPointBox->SetRequisition(sf::Vector2f(0, 0));
	mainInfoBox->Pack(buildingPointBox, false, false);

	infoWindow->Show(false);
}

void CityBuildingGui::activateCityBuildingGui(bool active)
{
	mainBox->Show(active);
	DisplayBuildTable(false);
}

void CityBuildingGui::AddToBox(sfg::Box::Ptr box, sfg::Desktop* desktop)
{
	box->Pack(mainBox);
	desktop->Add(infoWindow);
}

void CityBuildingGui::activateBuildMenu(bool active)
{
	DisplayBuildTable(active);
	if (!active)
	{
		removeBuildingInfo();
	}
}

void CityBuildingGui::removeBuildingInfo()
{
	infoWindow->Show(false);
}

void CityBuildingGui::displayBuilding(building* b)
{
	show(1);
	building temp = *b;

	int size = 0;
	for (size_t i = 0; i < outputForInfo.size(); i++)
	{
		outputForInfo[i]->Show(false);
	}

	for (size_t i = 0; i < temp.type.levels[0].outPut.resWares.size(); i++)
	{
		std::string tempString = (*resources->getResourceName())[temp.type.levels[0].outPut.resWares[i].resItem];
		std::string tempString2 = std::to_string(temp.type.levels[0].outPut.resWares[i].amount);
		outputForInfo[i]->SetText(tempString + ": " + tempString2);
		outputForInfo[i]->Show(true);
		size++;
	}
	for (size_t i = 0; i < temp.type.levels[0].outPut.tradeWares.size(); i++)
	{
		std::string tempString = (*resources->gettradeGoodsName())[temp.type.levels[0].outPut.tradeWares[i].tradeItem];
		std::string tempString2 = std::to_string(temp.type.levels[0].outPut.tradeWares[i].amount);
		outputForInfo[size]->SetText(tempString + ": " + tempString2);
		outputForInfo[size]->Show(true);
		size++;
	}
	int max = temp.type.levels[0].maxPawns;
	maxVillager->SetText("Max Workers: " + std::to_string(max));
	spin->SetRange(0, max);
	spin->SetValue(b->allowedEntities);
}

void CityBuildingGui::addBuildingButton(EventQueue<GUIEvent>* eventQueue, const sf::String& label)
{
	sfg::Button::Ptr temp = sfg::Button::Create(label);
	int i = counter;
	temp->GetSignal(sfg::Widget::OnLeftClick).Connect([this, eventQueue, i] {
		GUIEvent event;
		event.type = GUIEvent::BUILDING;
		event.building.id = i;
		eventQueue->pushEvent(event);
		this->infoMenu(true, i, 0);
	});

	buildTable->Attach(temp, sf::Rect<sf::Uint32>(counter % 2, (counter / 2) % (BuildingHandlerPerShow / 2), 1, 1));
	counter++;
	buttons.push_back(temp);
}

void CityBuildingGui::addBuildingButtonWithImage(const sf::String& fileName, EventQueue<GUIEvent>* eventQueue, const sf::String & label)
{
	sfg::Button::Ptr temp = sfg::Button::Create(label);
	sf::Image sfImage;
	sfImage.loadFromFile(fileName);
	auto image = sfg::Image::Create(sfImage);
	temp.get()->SetImage(image);

	buildTable->Attach(temp, sf::Rect<sf::Uint32>(counter % 2, (counter / 2) % BuildingHandlerPerShow, 1, 1));

	counter++;
	buttons.push_back(temp);
}

void CityBuildingGui::DisplayBuildTable(bool show)
{
	if (show)
	{
		int i = 0;
		for (auto b : buttons)
		{
			b->Show(i < BuildingHandlerPerShow);
			i++;
		}
	}
	else
	{
		for (auto b : buttons)
		{
			b->Show(false);
		}
	}
	menu->Show(!show);
	buildTableBox->Show(show);
}

void CityBuildingGui::moveBuildList(Direction direction)
{
	int nrButtons = buttons.size();
	if (direction == UP)
	{
		if (page != 0)
		{
			page--;
			for (size_t i = 0; i < nrButtons; i++)
			{
				buttons.at(i)->Show(i / BuildingHandlerPerShow == page);
			}
		}
	}
	else
	{
		if (page < nrButtons / BuildingHandlerPerShow)
		{
			page++;
			for (size_t i = 0; i < nrButtons; i++)
			{
				buttons.at(i)->Show(i / BuildingHandlerPerShow == page);
			}
		}
	}
}

void CityBuildingGui::setUpClickBuilding(sfg::Box::Ptr topBox, EventQueue<GUIEvent>* eventQueue)
{
	auto box = sfg::Box::Create();
	auto box2 = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL, 50);
	auto box3 = sfg::Box::Create();
	auto outputframe = sfg::Frame::Create("Output");
	outputframe->SetRequisition(sf::Vector2f(300, 1));
	auto villageFrame = sfg::Frame::Create("Village");

	topBox->Pack(outputframe, false, false);
	topBox->Pack(villageFrame, false, false);

	outputframe->Add(box);
	villageFrame->Add(box2);

	auto verBox1 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	verBox1->SetRequisition(sf::Vector2f(150, 110));
	auto verBox2 = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	for (size_t i = 0; i < 3; i++)
	{
		auto outputTemp = sfg::Label::Create("Wood: 20");
		verBox1->Pack(outputTemp, false, false);
		outputForInfo.push_back(outputTemp);
	}
	for (size_t i = 0; i < 3; i++)
	{
		auto outputTemp = sfg::Label::Create("Wood: 20");
		verBox2->Pack(outputTemp, false, false);
		outputForInfo.push_back(outputTemp);
	}
	box->Pack(verBox1, false, false);
	box->Pack(verBox2, false, false);

	maxVillager = sfg::Label::Create("Max workers = 5");

	auto ad = sfg::Adjustment::Create(0, 0, 5, 1, 3, 0);
	spin = sfg::SpinButton::Create(ad);
	spin->SetRequisition(sf::Vector2f(50, 50));
	box2->Pack(maxVillager, false, false);

	box2->Pack(spin, false, false);

	auto button = sfg::Button::Create("Upgrade");
	button->SetRequisition(sf::Vector2f(100, 50));
	box3->Pack(button, false, false);

	topBox->Pack(box3, false, false);
	spin->GetSignal(sfg::Widget::OnText).Connect([this, eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::ASSIGNED_WORKER_NUMBER_CHANGED;
		event.worker.newAmount = spin->GetValue();
		eventQueue->pushEvent(event);
	});
	spin->GetSignal(sfg::Widget::OnLeftClick).Connect([this, eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::ASSIGNED_WORKER_NUMBER_CHANGED;
		event.worker.newAmount = spin->GetValue();
		eventQueue->pushEvent(event);
	});
}

void CityBuildingGui::infoMenu(bool show, int buildingNr, int level)
{
	this->show(0);
	buildingFrame->SetLabel(buildings->at(buildingNr).levels[level].name);
	pawn->SetText(std::to_string(buildings->at(buildingNr).levels[level].maxPawns));
	int size = 0;
	for (size_t i = 0; i < output.size(); i++)
	{
		output[i]->Show(false);
	}
	for (size_t i = 0; i < cost.size(); i++)
	{
		cost[i]->Show(false);
	}

	for (size_t i = 0; i < buildings->at(buildingNr).levels[level].outPut.resWares.size(); i++)
	{
		std::string tempString = (*resources->getResourceName())[buildings->at(buildingNr).levels[level].outPut.resWares[i].resItem];
		std::string tempString2 = std::to_string(buildings->at(buildingNr).levels[level].outPut.resWares[i].amount);
		output[i]->SetText(tempString + ": " + tempString2);
		output[i]->Show(true);
		size++;
	}
	for (size_t i = 0; i < buildings->at(buildingNr).levels[level].outPut.tradeWares.size(); i++)
	{
		std::string tempString = (*resources->gettradeGoodsName())[buildings->at(buildingNr).levels[level].outPut.tradeWares[i].tradeItem];
		std::string tempString2 = std::to_string(buildings->at(buildingNr).levels[level].outPut.tradeWares[i].amount);
		output[size]->SetText(tempString + ": " + tempString2);
		output[size]->Show(true);
		size++;
	}
	size = 0;
	for (size_t i = 0; i < buildings->at(buildingNr).levels[level].price.resWares.size(); i++)
	{
		std::string tempString = (*resources->getResourceName())[buildings->at(buildingNr).levels[level].price.resWares[i].resItem];
		std::string tempString2 = std::to_string(buildings->at(buildingNr).levels[level].price.resWares[i].amount);
		cost[size]->SetText(tempString + ": " + tempString2);
		cost[size]->Show(true);
		size++;
	}
	for (size_t i = 0; i < buildings->at(buildingNr).levels[level].price.tradeWares.size(); i++)
	{
		std::string tempString = (*resources->gettradeGoodsName())[buildings->at(buildingNr).levels[level].price.tradeWares[i].tradeItem];
		std::string tempString2 = std::to_string(buildings->at(buildingNr).levels[level].price.tradeWares[i].amount);
		cost[size]->SetText(tempString + ": " + tempString2);
		cost[size]->Show(true);
		size++;
	}
	BuildingInfoLabel->SetText(buildings->at(buildingNr).levels[level].description);
}

void CityBuildingGui::show(int type)
{
	infoWindow->Show(true);
	if (type == 0)
	{
		buildingPointBox->Show(false);
		buildingBox->Show(true);
		underBox->Show(true);
	}
	if (type == 1)
	{
		buildingBox->Show(false);
		buildingPointBox->Show(true);
		underBox->Show(false);
	}
}