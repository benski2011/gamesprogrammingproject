#include "Civ.h"

bool Civ::willTrade(std::vector<TradeSum> player, std::vector<TradeSum> civTrade, std::string & text)
{
	float playerPrice = 0, civPrice = 0;
	for (size_t i = 0; i < player.size(); i++)
	{
		int f = 0;
		switch (player.at(i).type)
		{
		case 0: f = player.at(i).typeList;
			break;
		case 1: f = Resources::Resource::NUM_OF_RESOURCES;
			break;
		case 2: f = Resources::Resource::NUM_OF_RESOURCES + 1 + (1 * player.at(i).typeList);
			break;
		default:
			break;
		}
		playerPrice += goalState[f] * player.at(i).amount;
	}

	for (size_t i = 0; i < civTrade.size(); i++)
	{
		int f = 0;
		switch (civTrade.at(i).type)
		{
		case 0: f = civTrade.at(i).typeList;
			break;
		case 1: f = Resources::Resource::NUM_OF_RESOURCES;
			break;
		case 2: f = Resources::Resource::NUM_OF_RESOURCES + 1 + (1 * civTrade.at(i).typeList);
			break;
		default:
			break;
		}
		civPrice += goalState[f] * civTrade.at(i).amount;
	}

	luabridge::LuaRef sumNumbers = getGlobal(L, "wantTrade");
	luabridge::LuaRef trade = sumNumbers(playerPrice, civPrice, opinionOfPlayer, 0, 0);
	bool want = trade["trade"];
	opinionOfPlayer = trade["newOpinion"];

	return want;
}

std::string Civ::getName()
{
	return name;
}

int Civ::getGold()
{
	return resources.getGold();
}

Resources * Civ::getResource()
{
	return &resources;
}

Civ::Civ(std::string name, std::vector<Trait>* DifferentTraits)
{
	try
	{
		civState = READY;
		this->name = name;
		differentTraits = DifferentTraits;
		opinionOfPlayer = 75;
		reasonableOfferAcceptance = 0;
		reasonableOfferGiver = 0;
		negotiatingDifficulty = 3;
		governmentType = (Government)(rand() % NUM_OF_GOVERNMENTS);

		rank = 1;

		setUpTraits();
		setUpGoalState();

		L = luabridge::luaL_newstate();
		luaL_openlibs(L);

		luaL_dofile(L, "spillprogrammering-gruppe-4-rimworld-2.0/script.lua");

		luabridge::getGlobalNamespace(L)
			.beginNamespace("C")
			.beginClass <Civ>("CivClass")
			//.addConstructor <void(*) >()
			.addFunction("building", &Civ::createBuilding)
			.addFunction("food", &Civ::getFood)
			.addFunction("getNeeds", &Civ::getNeeds)
			.addFunction("getWants", &Civ::getWants)
			.addFunction("getRes", &Civ::getResourceBuildingHandler)
			.endClass()
			.beginNamespace("Res")
			.addVariable("wood", resources.resArray(Resources::WOOD))
			.addVariable("stone", resources.resArray(Resources::STONE))
			.addVariable("ironOre", resources.resArray(Resources::IRON_ORE))
			.addVariable("coal", resources.resArray(Resources::COAL))
			.addVariable("buildingMaterial", resources.resArray(Resources::BUILDING_MATERIAL))
			.endNamespace()
			.beginNamespace("ResPerTick")
			.addVariable("wood", resources.getResPerTick(Resources::WOOD))
			.addVariable("stone", resources.getResPerTick(Resources::STONE))
			.addVariable("ironOre", resources.getResPerTick(Resources::IRON_ORE))
			.addVariable("coal", resources.getResPerTick(Resources::COAL))
			.addVariable("buildingMaterial", resources.getResPerTick(Resources::BUILDING_MATERIAL))
			.endNamespace()
			.beginNamespace("Tg")
			.addVariable("fish", resources.tgArray(Resources::FISH))
			.addVariable("meat", resources.tgArray(Resources::MEAT))
			.addVariable("veggies", resources.tgArray(Resources::VEGGIES))
			.addVariable("grain", resources.tgArray(Resources::GRAIN))
			.addVariable("bread", resources.tgArray(Resources::BREAD))
			.addVariable("flour", resources.tgArray(Resources::FLOUR))
			.addVariable("planks", resources.tgArray(Resources::PLANKS))
			.addVariable("ironBar", resources.tgArray(Resources::IRON_BAR))
			.addVariable("bricks", resources.tgArray(Resources::BRICKS))
			.addVariable("leather", resources.tgArray(Resources::LEATHER))
			.addVariable("cloth", resources.tgArray(Resources::CLOTH))
			.addVariable("clothing", resources.tgArray(Resources::CLOTHING))
			.addVariable("wool", resources.tgArray(Resources::WOOL))
			.addVariable("clay", resources.tgArray(Resources::CLAY))
			.addVariable("ironTools", resources.tgArray(Resources::IRON_TOOLS))
			.addVariable("ironWeapons", resources.tgArray(Resources::IRON_WEAPONS))
			.addVariable("charcoal", resources.tgArray(Resources::CHARCOAL))
			.addVariable("firewood", resources.tgArray(Resources::FIREWOOD))
			.addVariable("furniture", resources.tgArray(Resources::FURNITURE))
			.addVariable("pottery", resources.tgArray(Resources::POTTERY))
			.addVariable("salt", resources.tgArray(Resources::SALT))
			.addVariable("spice", resources.tgArray(Resources::SPICE))
			.addVariable("tobacco", resources.tgArray(Resources::TOBACCO))
			.addVariable("cigars", resources.tgArray(Resources::CIGARS))
			.addVariable("beer", resources.tgArray(Resources::BEER))
			.addVariable("whiskey", resources.tgArray(Resources::WHISKEY))

			.endNamespace()
			.beginNamespace("TradePerTick")
			.addVariable("fish", resources.getTradePerTick(Resources::FISH))
			.addVariable("meat", resources.getTradePerTick(Resources::MEAT))
			.addVariable("veggies", resources.getTradePerTick(Resources::VEGGIES))
			.addVariable("grain", resources.getTradePerTick(Resources::GRAIN))
			.addVariable("bread", resources.getTradePerTick(Resources::BREAD))
			.addVariable("flour", resources.getTradePerTick(Resources::FLOUR))
			.addVariable("planks", resources.getTradePerTick(Resources::PLANKS))
			.addVariable("ironBar", resources.getTradePerTick(Resources::IRON_BAR))
			.addVariable("bricks", resources.getTradePerTick(Resources::BRICKS))
			.addVariable("leather", resources.getTradePerTick(Resources::LEATHER))
			.addVariable("cloth", resources.getTradePerTick(Resources::CLOTH))
			.addVariable("clothing", resources.getTradePerTick(Resources::CLOTHING))
			.addVariable("wool", resources.getTradePerTick(Resources::WOOL))
			.addVariable("clay", resources.getTradePerTick(Resources::CLAY))
			.addVariable("ironTools", resources.getTradePerTick(Resources::IRON_TOOLS))
			.addVariable("ironWeapons", resources.getTradePerTick(Resources::IRON_WEAPONS))
			.addVariable("charcoal", resources.getTradePerTick(Resources::CHARCOAL))
			.addVariable("firewood", resources.getTradePerTick(Resources::FIREWOOD))
			.addVariable("furniture", resources.getTradePerTick(Resources::FURNITURE))
			.addVariable("pottery", resources.getTradePerTick(Resources::POTTERY))
			.addVariable("salt", resources.getTradePerTick(Resources::SALT))
			.addVariable("spice", resources.getTradePerTick(Resources::SPICE))
			.addVariable("tobacco", resources.getTradePerTick(Resources::TOBACCO))
			.addVariable("cigars", resources.getTradePerTick(Resources::CIGARS))
			.addVariable("beer", resources.getTradePerTick(Resources::BEER))
			.addVariable("whiskey", resources.getTradePerTick(Resources::WHISKEY))
			.endNamespace()
			.beginNamespace("needs")
			.addVariable("food", &needs[0])
			.addVariable("clothing", &needs[1])
			.addVariable("heating", &needs[2])
			.addVariable("tools", &needs[3])
			.endNamespace()
			.beginNamespace("wants")
			.addVariable("salt", &wants[0])
			.addVariable("spice", &wants[1])
			.addVariable("pottery", &wants[2])
			.addVariable("furniture", &wants[3])
			.addVariable("ironWeapons", &wants[4])
			.addVariable("whiskey", &wants[5])
			.addVariable("cigars", &wants[6])
			.endNamespace()
			.beginNamespace("resCalc")
			.addVariable("wood", &resource[0])
			.addVariable("stone", &resource[1])
			.addVariable("ironOre", &resource[2])
			.addVariable("buildingMaterial", &resource[3])
			.endNamespace()
			.addVariable("Gold", resources.goldPointer())
			.endNamespace();

		luabridge::setGlobal(L, this, "Civ");
		lua_pcall(L, 0, 0, 0);
		luabridge::LuaRef setup = getGlobal(L, "Setup");
		setup();
	}
	catch (const std::exception& e)
	{
		printf("%s \n", e.what());
	}
}

sfg::Image::Ptr Civ::getIcon()
{
	return icon;
}

std::string Civ::getInfoString()
{
	return infoString;
}

void Civ::update()
{
	resources.update();
	luabridge::LuaRef decideAIMove = getGlobal(L, "civDecition");
	switch (civState)
	{
	case Civ::WAITING:

		break;
	case Civ::BUILDING:
		if (timer > 0)
		{
			timer--;
		}
		else
		{
			civState = READY;
		}
		break;
	case Civ::TRADING:
		break;
	case Civ::READY:
		decideAIMove();
		break;
	default:
		break;
	}
}

void Civ::setVers(Vars var, float amount)
{
	switch (var)
	{
	case ROA: reasonableOfferAcceptance += amount;
		break;
	case ROG: reasonableOfferGiver += amount;
		break;
	case ND: negotiatingDifficulty += amount;
		break;
	default:
		break;
	}
}

float calculateDemand(int perTick, int value)
{
	float demand = -100;
	if (value != 0)
	{
		demand = (float)perTick / value;
	}
	return demand;
}

void Civ::getNeeds()
{
	int foodPerTick = 0;
	for (size_t i = 0; i < 5; i++)
	{
		foodPerTick += *resources.getTradePerTick(i);
	}
	needs[0] = calculateDemand(foodPerTick, getFood());

	int  clothingPerTick = *resources.getTradePerTick(Resources::CLOTHING);
	needs[1] = calculateDemand(clothingPerTick, *resources.tgArray(Resources::CLOTHING));

	int heatingPerTick = *resources.getResPerTick(Resources::COAL) + *resources.getTradePerTick(Resources::FIREWOOD) + *resources.getTradePerTick(Resources::CHARCOAL);
	needs[2] = calculateDemand(heatingPerTick, (*resources.resArray(Resources::COAL) + *resources.tgArray(Resources::FIREWOOD) + *resources.tgArray(Resources::CHARCOAL)));

	needs[3] = calculateDemand(*resources.getResPerTick(Resources::IRON_TOOLS), *resources.tgArray(Resources::IRON_TOOLS));
}

void Civ::getWants()
{
	wants[0] = calculateDemand(*resources.getTradePerTick(Resources::SALT), *resources.tgArray(Resources::SALT));
	wants[1] = calculateDemand(*resources.getTradePerTick(Resources::SPICE), *resources.tgArray(Resources::SPICE));
	wants[2] = calculateDemand(*resources.getTradePerTick(Resources::POTTERY), *resources.tgArray(Resources::POTTERY));
	wants[3] = calculateDemand(*resources.getTradePerTick(Resources::FURNITURE), *resources.tgArray(Resources::FURNITURE));
	wants[4] = calculateDemand(*resources.getTradePerTick(Resources::IRON_WEAPONS), *resources.tgArray(Resources::IRON_WEAPONS));
	wants[5] = calculateDemand(*resources.getTradePerTick(Resources::WHISKEY), *resources.tgArray(Resources::WHISKEY));
	wants[6] = calculateDemand(*resources.getTradePerTick(Resources::CIGARS), *resources.tgArray(Resources::CIGARS));
}

void Civ::setUpTraits()
{
	for (size_t i = 0; i < 3; i++)
	{
		int t = (rand() % 2) + (2 * i);
		traits.push_back(t);
	}
	for (size_t i = 0; i < traits.size(); i++)
	{
		int g = traits.at(i);
		auto f = std::find_if(std::begin(*differentTraits), std::end(*differentTraits), [g](Trait t) { return(t.id == g); });
		f->TraitFunction(this);
	}
}

void Civ::setUpGoalState()
{
	float temp = 3;
	// 0 to NUM_OF_RESOURCES is resources, the next is gold and after tradegoods
	for (size_t i = 0; i < Resources::NUM_OF_RESOURCES; i++)
	{
		goalState.push_back(temp);
	}
	temp = 0.01;
	if (traits[0] == differentTraits->at(1).id)
	{
		temp = 0.03;
	}
	goalState.push_back(temp);
	for (size_t i = 0; i < Resources::NUM_OF_TRADEGOODS; i++)
	{
		temp = 1;
		goalState.push_back(temp);
	}
}

void Civ::createBuilding(int i, int f)
{
	//printf("Building %i %i\n", i, f);
	civState = BUILDING;
	timer = 100;
	if (f == 2)
	{
		resources.addTradeGoodsPerTick(Resources::CLOTHING, 100);
		//printf("per tick = %i \n", *resources.getTradePerTick(Resources::CLOTHING));
		//printf("value = %i\n", resources.getTradeGoods(Resources::CLOTHING));
	}
}

void Civ::getResourceBuildingHandler()
{
	resource[0] = calculateDemand(*resources.getResPerTick(Resources::WOOD), *resources.resArray(Resources::WOOD));
	resource[1] = calculateDemand(*resources.getResPerTick(Resources::STONE), *resources.resArray(Resources::STONE));
	resource[2] = calculateDemand(*resources.getResPerTick(Resources::IRON_ORE), *resources.resArray(Resources::IRON_ORE));
	resource[3] = calculateDemand(*resources.getResPerTick(Resources::BUILDING_MATERIAL), *resources.resArray(Resources::BUILDING_MATERIAL));
}

int Civ::getFood()
{
	int food = 0;
	for (size_t i = 0; i < 5; i++)
	{
		food += *resources.tgArray(i);
	}
	return food;
}