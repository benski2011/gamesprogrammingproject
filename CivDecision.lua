
SetResources = {
	Resources = {
		wood = 0,
		stone = 0,
		ironOre = 0,
		coal = 0,
		buildingMaterial = 0
	},
	tradeGoods = {
		food = {
			fish = 0,
			meat = 0,
			veggies = 0,
			grain = 0,
			bread = 0
		},
		flour = 0,
		
		planks = 0,
		ironBar = 0,
		bricks = 0,
		leather = 0,
		cloth = 0,
		clothing = 0,
		wool = 0,
		clay = 0,
		
		ironTools = 0,
		ironWeapons = 0,
		charcoal = 0 ,
		firewood = 0,
		
		furniture = 0,
		pottery = 0,
		salt = 0,
		spice = 0,
		tobacco = 0,
		cigars = 0,
		whiskey = 0
		
	}
}

needs = {
	food = {
		fish = 0,
		meat = 1,
		veggies = 2,
		grain = 3,
		bread = 4
	},
	clothing = 11,
	heating = {
		coal = 3,
		firewood = 16,
		charcoal = 0
	}
	
}

	



function foodBuilding()
	if not Civ:building(0, 1) then
		findBuildingMats()
	end 
end

function clothingBuilding()
	if not Civ:building(0, 2) then
		findBuildingMats()
	end 
end

function heatingBuilding()
	if not Civ:building(0, 3) then
		findBuildingMats()
	end 
end

function ironToolsBuilding()
	if not Civ:building(0, 4) then
		findBuildingMats()
	end 
end
function saltBuilding()
	if not Civ:building(0, 5) then
		findBuildingMats()
	end 
end
function spicesBuilding()
	if not Civ:building(0, 6) then
		findBuildingMats()
	end 
end

function potteriesBuilding()
	if not Civ:building(0, 7) then
		findBuildingMats()
	end 
end

function furnitureBuilding()
	if not Civ:building(0, 8) then
		findBuildingMats()
	end 
end

function ironWeaponBuilding()
	if not Civ:building(0, 9) then
		findBuildingMats()
	end 
end

function whiskeyBuilding()
	if not Civ:building(0, 10) then
		findBuildingMats()
	end 
end

function cigarBuilding()
	if not Civ:building(0, 11) then
		findBuildingMats()
	end 
end

function goldsBuilding()
	if not Civ:building(1, 0) then
		findBuildingMats()
	end 
end

function resourceBuilding()
	if c.resCalc.wood < 0 then
		if not Civ:building(2, 0) then
			findBuildingMats()
		end 
		return true
	elseif c.resCalc.stone < 0 then 
		if not Civ:building(2, 1) then
			findBuildingMats()
		end 
		return true
	elseif c.resCalc.ironOre < 0 then
		if not Civ:building(2, 3) then
			findBuildingMats()
		end 
		return true
	elseif c.resCalc.buildingMaterial < 0 then
		if not Civ:building(2, 4) then
			findBuildingMats()
		end
		return true
	end
	return false
end

function findBuildingMats()


end

function civDecition()
	Civ:getNeeds()
	Civ:getRes()
	Civ:getWants()
	 
	
	if C.needs.food < -0.2 then
		foodBuilding()
	elseif C.needs.clothing < -0.2 then
		clothingBuilding()
	elseif C.needs.heating < -0.2 then
		heatingBuilding()
	elseif C.needs.tools <  -0.2 then
		ironToolsBuilding()
	elseif resourceBuilding() then
	elseif C.wants.salt < 0.1 then
		saltBuilding()
	elseif C.wants.spice < 0.1 then
		spiceBuilding()
	elseif C.wants.pottery < 0.1 then
		potteriesBuilding()
	elseif C.wants.furniture < 0.1 then
		furnitureBuilding()
	elseif C.wants.ironWeapons < 0.1 then
		ironWeaponBuilding()
	elseif C.wants.whiskey < 0.1 then
		whiskeyBuilding()
	elseif C.wants.cigar < 0.1 then
		cigarBuilding()                
	else
		goldsBuilding()
	end
	
end

