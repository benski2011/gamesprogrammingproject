#include "CivHandler.h"

CivHandler::CivHandler(int amountofCivs)
{
	InitTraits();

	std::ifstream file("./spillprogrammering-gruppe-4-rimworld-2.0/data/CivName.data");
	std::vector<std::string> temp;

	srand(time(NULL));

	if (!file.is_open())
	{
		printf("can't load file\n");
	}
	while (!file.eof())
	{
		std::string temp2;
		std::getline(file, temp2);
		temp.push_back(temp2);
	}

	civs.reserve(amountofCivs);

	for (int i = 0; i < amountofCivs; i++)
	{
		int f = rand() % temp.size();
		std::string randomName = temp[f];
		civs.emplace_back(randomName, &traits);
		temp[f] = temp.back();
		temp.pop_back();
	}
}

std::vector<std::string> CivHandler::getNames()
{
	std::vector<std::string> temp;
	for (auto civ : civs)
	{
		temp.push_back(civ.getName());
	}
	return temp;
}

std::vector<Civ>* CivHandler::getcivs()
{
	return &civs;
}

void CivHandler::update()
{
	for (auto& civ : civs)
	{
		civ.update();
	}
}

void CivHandler::InitTraits()
{
	traitId = 0;
	Trait temp;
	temp.id = traitId++;
	temp.name = "Honest";
	temp.TraitFunction = [](Civ* c) {
		c->setVers(Civ::Vars::ROA, 0.2f);
		c->setVers(Civ::Vars::ROG, 0.1f);
	};
	traits.push_back(temp);

	temp.id = traitId++;
	temp.name = "Dishonest";
	temp.TraitFunction = [](Civ* c) {
		c->setVers(Civ::Vars::ROA, -0.2f);
		c->setVers(Civ::Vars::ROG, -0.1f);
	};
	traits.push_back(temp);

	temp.id = traitId++;
	temp.name = "Charitable";
	temp.TraitFunction = [](Civ* c) {c->setVers(Civ::Vars::ROA, 0.1f); };
	traits.push_back(temp);

	temp.id = traitId++;
	temp.name = "Greedy";
	temp.TraitFunction = [](Civ* c) {c->setVers(Civ::Vars::ROA, -0.1f); };
	traits.push_back(temp);

	temp.id = traitId++;
	temp.name = "GrandNegotiator";
	temp.TraitFunction = [](Civ* c) {c->setVers(Civ::Vars::ND, 1); };
	traits.push_back(temp);

	temp.id = traitId++;
	temp.name = "NaiveAppeaser";
	temp.TraitFunction = [](Civ* c) {c->setVers(Civ::Vars::ND, -1); };
	traits.push_back(temp);
}