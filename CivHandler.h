#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <time.h>

#include "Civ.h"

class Civ;
struct Trait;

class CivHandler
{
public:
	/**
	* Constructor for the civhandler class. Reads a random name from file, and instantiates the civs .
	* @ param the amount of civs you want to have
	*/
	CivHandler(int amountofCivs);
	/**
	* gets all the names of every civ
	*/
	std::vector<std::string> getNames();
	/**
	* gets a pointer to the vector of civ
	*/
	std::vector<Civ>* getcivs();
	/**
	* updates all civs
	*/
	void update();
private:
	void InitTraits();

	std::vector<Civ> civs;
	std::vector<Trait> traits;
	int traitId;
};
