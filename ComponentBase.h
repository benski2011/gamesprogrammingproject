#pragma once

#include <string>
#include <cstddef>

class ComponentBase
{
public:
	/**
	 * @param Sets parentId, used to access parent
	 */
	ComponentBase(std::size_t id);
	ComponentBase();
	~ComponentBase();

	/**
	 * RecieveMessage functionf for overloading.
	 * @param Message recieved.
	 */
	void recieveMessage(char message[], void* data);

	/**
	 * Gets the parentId
	 * @return parent id
	 */
	inline std::size_t& getParent()
	{
		return parentId;
	}

protected:
	std::size_t parentId = std::numeric_limits<std::size_t>::max();
};
