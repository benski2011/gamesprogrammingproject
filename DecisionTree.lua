math.randomseed(os.time())

nodeNmber = 1

--DecisionTree--
DecisionTree = {}
DecisionTree.__index = DecisionTree

function DecisionTree:create()
	local dct = {}             -- our new object
	setmetatable(dct,DecisionTree)  -- make Account handle lookup
	dct.nodes = {}
	dct.nodeSize = 0
	return dct
	
end

function DecisionTree:addNode(node)
	self.nodeSize = self.nodeSize + 1
	self.nodes[self.nodeSize] = node
end

--LeafNode--
LeafNode = {}
LeafNode.__index = LeafNode
function LeafNode:create(funckion)
	local lfnd = {}          
	setmetatable(lfnd,LeafNode)  
	lfnd.funckion = funckion
	return lfnd
end

function LeafNode:activate()
	return self.funckion()
end

--CompositeNode--
CompositeType = {
	Sequence = 1,
	Selector = 2,
	randomSelectors  = 3,
	randomSequences = 4 }
CompositeNode = {}
CompositeNode.__index = CompositeNode
function CompositeNode:create(CompType)
	local cmnd = {}          
	setmetatable(cmnd,CompositeNode) 
	cmnd.children = {}
	cmnd.childSize = 0
	cmnd.type = CompType
	leaf = LeafNode:create()
	return cmnd
end

function CompositeNode:addChild(child)
	self.childSize = self.childSize + 1
	self.children[self.childSize] = child
end

function CompositeNode:activate()
	if self.type == CompositeType.Sequence then
		for i = 1, self.childSize, 1 do
			if not self.children[i]:activate() then 
				return false
			end
		end
		return true
	end
	if self.type == CompositeType.Selector then
		for i = 1, self.childSize, 1 do
			if self.children[i]:activate() then 
				return true
			end
		end
		return false
	end
	if self.type == CompositeType.randomSelectors then
		randomArray = {}
		for i = 1, self.childSize, 1 do
			randomArray[i] = i
		end
		randomSize = self.childSize
		
		for i = 1, self.childSize, 1 do
			index = math.random(randomSize)
			randomSize = randomSize - 1
			table.remove(randomArray, index)
			if self.children[index]:activate() then
				return true
			end
		end
		return false
	end
	if self.type == CompositeType.randomSequences then
		randomArray = {}
		for i = 1, self.childSize, 1 do
			randomArray[i] = i
		end
		randomSize = self.childSize
			
		for i = 1, self.childSize, 1 do
			index = math.random(randomSize)
			randomSize = randomSize - 1
			table.remove(randomArray, index)
			
			if not self.children[index]:activate() then
				return false
			end
		end
		
		return true
	end
end

--DecoratorNode--
DecoratorType = {
	Inverter = 1,
	Succeeder = 2 
}
DecoratorNode = {}
DecoratorNode.__index = DecoratorNode
function DecoratorNode:create(decType)
	local dcnd = {}          
	setmetatable(dcnd,DecoratorNode)
	dcnd.node
	dcnd.type = decType
	
	
	return dcnd
end

function DecoratorNode:setChild(child)
	self.node = child
	
end

function DecoratorNode:activate()
	if self.type == DecoratorType.Inverter then
		return not self.node:activate()
	
	elseif self.type == DecoratorType.Succeeder then
		self.node:activate()
		return true
	
	end
end


--function Account:withdraw(amount)
--   self.balance = self.balance - amount
--end

f = function() 
	print("hai")
	return true 
end

foofighters = function() 
	print("iah")
	return false 
end



dec = DecisionTree:create()
node = CompositeNode:create(CompositeType.Sequence)
node2 = CompositeNode:create(CompositeType.Sequence)
node:addChild(node2)
node3 = CompositeNode:create(CompositeType.Selector)
node2:addChild(node3)
node4 = CompositeNode:create(CompositeType.Selector)
node2:addChild(node4)
node5 = CompositeNode:create(CompositeType.randomSelectors)
node2:addChild(node5)
node6 = CompositeNode:create(CompositeType.randomSelectors)
node5:addChild(node6)
node7 = CompositeNode:create(CompositeType.randomSelectors)
node5:addChild(node7)
node8 = CompositeNode:create(CompositeType.randomSequences)
node5:addChild(node8)
node9 = CompositeNode:create(CompositeType.randomSequences)
node5:addChild(node9)
leaf = LeafNode:create(f)
node:addChild(leaf)
leaf2 = LeafNode:create(f)
node2:addChild(leaf2)
leaf3 = LeafNode:create(f)
node3:addChild(leaf3)
leaf4 = LeafNode:create(f)
node4:addChild(leaf4)
leaf5 = LeafNode:create(f)
node5:addChild(leaf5)
leaf6 = LeafNode:create(f)
node6:addChild(leaf6)
leaf7 = LeafNode:create(f)
node7:addChild(leaf7)
leaf8 = LeafNode:create(f)
node8:addChild(leaf8)
leaf9 = LeafNode:create(f)
leaf10 = LeafNode:create(foofighters)

node9:addChild(leaf9)

node:activate()

