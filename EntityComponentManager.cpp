#include "EntityComponentManager.h"

EntityComponentManager::EntityComponentManager()
{
}

std::size_t EntityComponentManager::createEntity()
{
	GameEntity newEntity;

	for (std::size_t i = 0; i < NUMBER_OF_COMPONENTS; ++i)
	{
		newEntity.components[i] = invalidID;
	}

	if (freeIdPool.size() == 0)
	{
		newEntity.id = entityIdTranslationTable.size();
		entityIdTranslationTable.push_back(entities.size());
	}
	else
	{
		newEntity.id = freeIdPool.back();
		freeIdPool.pop_back();
		entityIdTranslationTable[newEntity.id] = entities.size();
	}

	entities.push_back(newEntity);

	return newEntity.id;
}

void EntityComponentManager::deleteEntity(std::size_t entityId)
{
	assert(entityId < entityIdTranslationTable.size());
	assert(entityIdTranslationTable[entityId] != invalidID);
	std::size_t id = translateID(entityId);
	std::size_t lastEntityTranslationTableId = entities[entities.size() - 1].id;

	for (std::size_t i = 0; i < NUMBER_OF_COMPONENTS; ++i)
	{
		if (entities[id].components[i] != invalidID)
		{
			switch (i)
			{
			case GRAPHICS_COMPONENT:
			{
				std::size_t swapComponentParentId = translateID(graphicsComponents[graphicsComponents.size() - 1].getParent());

				graphicsComponents[entities[id].components[i]] = graphicsComponents[graphicsComponents.size() - 1];
				entities[swapComponentParentId].components[i] = entities[id].components[i];

				graphicsComponents.pop_back();
				break;
			}

			case INPUT_COMPONENT:
			{
				std::size_t swapComponentParentId = translateID(inputComponents[inputComponents.size() - 1].getParent());

				inputComponents[entities[id].components[i]] = inputComponents[inputComponents.size() - 1];
				entities[swapComponentParentId].components[i] = entities[id].components[i];

				inputComponents.pop_back();
				break;
			}

			case PHYSICS_COMPONENT:
			{
				std::size_t swapComponentParentId = translateID(physicsComponents[physicsComponents.size() - 1].getParent());

				physicsComponents[entities[id].components[i]] = physicsComponents[physicsComponents.size() - 1];
				entities[swapComponentParentId].components[i] = entities[id].components[i];

				physicsComponents.pop_back();
				break;
			}

			case POSITION_COMPONENT:
			{
				std::size_t swapComponentParentId = translateID(positionComponents[positionComponents.size() - 1].getParent());

				positionComponents[entities[id].components[i]] = positionComponents[positionComponents.size() - 1];
				entities[swapComponentParentId].components[i] = entities[id].components[i];

				positionComponents.pop_back();
				break;
			}

			case PATHING_COMPONENT:
			{
				std::size_t swapComponentParentId = translateID(pathingComponents[pathingComponents.size() - 1].getParent());

				pathingComponents[entities[id].components[i]] = pathingComponents[pathingComponents.size() - 1];
				entities[swapComponentParentId].components[i] = entities[id].components[i];

				pathingComponents.pop_back();
				break;
			}

			default:
				assert(false);
				break;
			}
		}
	}

	entityIdTranslationTable[entities[entities.size() - 1].id] = id;
	entityIdTranslationTable[entityId] = invalidID;

	freeIdPool.push_back(entityId);

	entities[id] = entities[entities.size() - 1];
	entities.pop_back();
}

ComponentBase* EntityComponentManager::createComponent(componentTypes type, std::size_t entityID, std::string filePath)
{
	assert(entityID < entityIdTranslationTable.size());
	assert(translateID(entityID) != invalidID);
	std::size_t id = translateID(entityID);

	switch (type)
	{
	case GRAPHICS_COMPONENT:
	{
		GraphicComponent newGComponent(id);

		entities[id].components[GRAPHICS_COMPONENT] = graphicsComponents.size();

		graphicsComponents.push_back(newGComponent);
		return &graphicsComponents.back();
		break;
	}
	case INPUT_COMPONENT:
	{
		InputComponent newIComponent(id);

		entities[id].components[INPUT_COMPONENT] = inputComponents.size();

		inputComponents.push_back(newIComponent);
		return &inputComponents.back();
		break;
	}
	case PHYSICS_COMPONENT:
	{
		PhysicsComponent newPhyComponent(id);

		entities[id].components[PHYSICS_COMPONENT] = physicsComponents.size();

		physicsComponents.push_back(newPhyComponent);
		return &physicsComponents.back();
		break;
	}

	case POSITION_COMPONENT:
	{
		PositionComponent newPosComponent(id);

		entities[id].components[POSITION_COMPONENT] = positionComponents.size();

		positionComponents.push_back(newPosComponent);
		return &positionComponents.back();
		break;
	}

	case PATHING_COMPONENT:
	{
		PathingComponent newPathingComponent(id);

		entities[id].components[PATHING_COMPONENT] = pathingComponents.size();

		pathingComponents.push_back(newPathingComponent);
		return &pathingComponents.back();
		break;
	}
	default:
		assert(false);
		break;
	}

	return nullptr;
}

std::vector<GraphicComponent>* EntityComponentManager::getGraphicComponents()
{
	return &graphicsComponents;
}

std::vector<PhysicsComponent>* EntityComponentManager::getPhysicComponents()
{
	return &physicsComponents;
}

std::vector<InputComponent>* EntityComponentManager::getInputComponents()
{
	return &inputComponents;
}

ComponentBase* EntityComponentManager::getComponent(componentTypes type, std::size_t entityId)
{
	assert(translateID(entityId) != invalidID);
	assert(entities[translateID(entityId)].components[type] != invalidID);

	std::size_t componentIndex = entities[translateID(entityId)].components[type];

	switch (type)
	{
	case GRAPHICS_COMPONENT:
		return &graphicsComponents[componentIndex];
		break;

	case INPUT_COMPONENT:
		return &inputComponents[componentIndex];
		break;

	case PHYSICS_COMPONENT:
		return &physicsComponents[componentIndex];
		break;

	case POSITION_COMPONENT:
		return &positionComponents[componentIndex];
		break;

	default:
		assert(false);
		return nullptr;
		break;
	}
}

void EntityComponentManager::setEntityPos(std::size_t id, glm::vec2& newPos)
{
	positionComponents[entities[translateID(id)].components[POSITION_COMPONENT]].setPosition(newPos);
}

glm::vec2 EntityComponentManager::getEntityPos(std::size_t& entityId)
{
	glm::vec3 pos = positionComponents[entities[translateID(entityId)].components[POSITION_COMPONENT]].getPosition();

	return glm::vec2(pos.x, pos.z);
}

void EntityComponentManager::sendMessageToComponent(componentTypes type, std::size_t& entityId, char message[], void* data)
{
	assert(entityId < entityIdTranslationTable.size());
	if (entities[translateID(entityId)].components[type] != invalidID)
	{
		std::size_t componentIndex = entities[translateID(entityId)].components[type];
		switch (type)
		{
		case GRAPHICS_COMPONENT:
			graphicsComponents[componentIndex].recieveMessage(message, data);
			break;

		case INPUT_COMPONENT:
			inputComponents[componentIndex].recieveMessage(message, data);
			break;

		case PHYSICS_COMPONENT:
			physicsComponents[componentIndex].recieveMessage(message, data);
			break;

		case POSITION_COMPONENT:
			positionComponents[componentIndex].recieveMessage(message, data);
			break;

		case PATHING_COMPONENT:
			pathingComponents[componentIndex].recieveMessage(message, data);
			break;

		default:
			assert(false);
			break;
		}
	}
}

void EntityComponentManager::writeToFile()
{
	std::ofstream file("./EntitiesAndComponents.dnd", std::ofstream::binary);

	std::size_t size;
	size = entities.size();
	file.write((char*)&size, sizeof(size));
	file.write((char*)&entities[0], sizeof(entities[0]) * entities.size());

	size = entityIdTranslationTable.size();
	file.write((char*)&size, sizeof(size));
	file.write((char*)&entityIdTranslationTable[0], sizeof(entityIdTranslationTable[0]) * entityIdTranslationTable.size());

	size = graphicsComponents.size();
	file.write((char*)&size, sizeof(size));
	for (auto i = 0; i < size; ++i)
	{
		file.write((char*)&graphicsComponents[i], sizeof(GraphicComponent) - sizeof(sf::Sprite));
	}

	for (std::size_t i = 0; i < size; ++i)
	{
		file.write((char*)&EntityComponentManager::getInstance().getEntityPos(graphicsComponents[i].getParent()), sizeof(glm::vec2));
	}

	size = inputComponents.size();
	file.write((char*)&size, sizeof(size));
	file.write((char*)&inputComponents[0], sizeof(inputComponents[0]) * inputComponents.size());

	size = physicsComponents.size();
	file.write((char*)&size, sizeof(size));
	file.write((char*)&physicsComponents[0], sizeof(physicsComponents[0]) * physicsComponents.size());

	size = positionComponents.size();
	file.write((char*)&size, sizeof(size));
	file.write((char*)&positionComponents[0], sizeof(positionComponents[0]) * positionComponents.size());

	size = pathingComponents.size();
	file.write((char*)&size, sizeof(size));
	std::size_t pathSize;

	for (auto i = 0; i < size; ++i)
	{
		pathSize = pathingComponents[i].path.size();
		file.write((char*)&pathingComponents[i], sizeof(PathingComponent) - sizeof(std::vector<glm::vec2>));
		file.write((char*)&pathSize, sizeof(pathSize));
		if (pathSize != 0)
		{
			file.write((char*)&pathingComponents[i].path[0], sizeof(glm::vec2) * pathSize);
		}
	}
}

void EntityComponentManager::loadFromFile()
{
	std::ifstream file("./EntitiesAndComponents.dnd", std::ifstream::binary);

	std::size_t size;
	file.read((char*)&size, sizeof(size));
	entities.resize(size);
	file.read((char*)&entities[0], size * sizeof(GameEntity));

	file.read((char*)&size, sizeof(size));
	entityIdTranslationTable.resize(size);
	file.read((char*)&entityIdTranslationTable[0], size * sizeof(std::size_t));

	file.read((char*)&size, sizeof(size));
	graphicsComponents.resize(size);
	for (auto i = 0; i < size; ++i)
	{
		file.read((char*)&graphicsComponents[i], sizeof(GraphicComponent) - sizeof(sf::Sprite));
	}

	glm::vec2 pos;

	for (std::size_t i = 0; i < size; ++i)
	{
		file.read((char*)&pos, sizeof(pos));
		graphicsComponents[i].recieveMessage("NewPosition", (void*)&pos);
	}

	file.read((char*)&size, sizeof(size));
	inputComponents.resize(size);
	file.read((char*)&inputComponents[0], size * sizeof(InputComponent));

	file.read((char*)&size, sizeof(size));
	physicsComponents.resize(size);
	file.read((char*)&physicsComponents[0], size * sizeof(PhysicsComponent));

	file.read((char*)&size, sizeof(size));
	positionComponents.resize(size);
	file.read((char*)&positionComponents[0], size * sizeof(PositionComponent));

	file.read((char*)&size, sizeof(size));
	pathingComponents.resize(size);

	std::size_t pathSize = 0;
	for (auto i = 0; i < size; ++i)
	{
		file.read((char*)&pathingComponents[i], sizeof(PathingComponent) - sizeof(std::vector<glm::vec2>));
		file.read((char*)&pathSize, sizeof(pathSize));
		if (pathSize != 0)
		{
			pathingComponents[i].path.resize(pathSize);
			file.read((char*)&pathingComponents[i].path[0], sizeof(glm::vec2) * pathSize);
		}
	}
}

std::size_t& EntityComponentManager::translateID(std::size_t& entityId)
{
	assert(entityId < entityIdTranslationTable.size());
	return entityIdTranslationTable[entityId];
}

void EntityComponentManager::printAllVectorContents()
{
	for (int i = 0; i < entities.size(); ++i)
	{
		printf("\n\nEntity: %i with components: ", entities[i].id);
		for (int j = 0; j < NUMBER_OF_COMPONENTS; ++j)
		{
			if (entities[i].components[j] != invalidID)
			{
				switch (j)
				{
				case GRAPHICS_COMPONENT:
					printf("\nGraphic component at index %i, with parent %i", entities[i].components[j], graphicsComponents[entities[i].components[j]].getParent());
					break;

				case INPUT_COMPONENT:
					printf("\nInput component at index %i, with parent %i", entities[i].components[j], inputComponents[entities[i].components[j]].getParent());
					break;

				case PHYSICS_COMPONENT:
					printf("\nPhysics component at index %i, with parent %i", entities[i].components[j], inputComponents[entities[i].components[j]].getParent());
					break;

				default:
					break;
				}
			}
		}

		printf("\n\nAgain, entityId is %i", entities[i].id);
		printf("\nInternal index is supposed to be %i", entityIdTranslationTable[entities[i].id]);
	}

	for (int j = 0; j < entityIdTranslationTable.size(); ++j)
	{
		printf("\nTranslator table index %i corresponds to entity index %i", j, entityIdTranslationTable[j]);
	}
}

std::size_t EntityComponentManager::getSize()
{
	return entities.size();
}