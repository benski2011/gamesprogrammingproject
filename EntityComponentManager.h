#pragma once

//#define NDEBUG
#include <assert.h>

#include <fstream>
#include <string>

#include "GameEntity.h"

#include "ComponentBase.h"
#include "GraphicComponent.h"
#include "InputComponent.h"
#include "PhysicsComponent.h"
#include "PositionComponent.h"
#include "PathingComponent.h"

class GraphicComponent;
class PhysicsComponent;
class InputComponent;
class PositionComponent;
class PathingComponent;

/**
 * The manager for all entities and components.
 * EntityComponentManager is a singleton and is the only class
 * that should create entites and components.
 */
class EntityComponentManager
{
public:

	/**
	 *
	 * Gets the instance of the singleton.
	 * Instance is created the first time this is called.
	 *
	 * @return Instance of singleton
	 */
	inline static EntityComponentManager& getInstance()
	{
		static EntityComponentManager instance;

		return instance;
	}

	/**
	 * Creates an entity.
	 * Creates a new entity. Entity is added to the vector
	 * of entities.
	 *
	 * @see createComponent()
	 * @see setEntityPos()
	 * @see getEntityPos()
	 * @return Id
	 */
	std::size_t createEntity();

	/**
	 * Deletes a entity and all it's components.
	 *
	 * @param Id of entity to delete
	 * @see createEntity
	 */
	void deleteEntity(std::size_t entityId);

	/**
	 * Creates a new component of specified type.
	 * Creates a new component type of a specified type
	 * belonging to a specified entity. Component is added
	 * to the respective vector container.
	 *
	 * @param Type of component, a enum.
	 * @param Id for the entity this component will belong to.
	 * @param Filepath if data files are necessary for a component to function, currently only used for graphic component.
	 * @see createEntity()
	 * @see getComponent()
	 */
	ComponentBase* createComponent(componentTypes type, std::size_t entityID, std::string filePath = "");

	/**
	 * Gets the vector containing all graphic components.
	 * Gets the vector containing all graphic components. It
	 * is used to loop over all graphic components during drawing
	 * as efficient as possible.
	 *
	 * @see createComponent()
	 * @return Pointer to vector with all graphic components
	 */
	std::vector<GraphicComponent>* getGraphicComponents();

	/**
	* Gets the vector containing all physic components.
	* Gets the vector containing all physic components. It
	* is used to loop over all physic components during updates
	* as efficient as possible.
	*
	* @see createComponent()
	* @return Pointer to vector with all physic components
	*/
	std::vector<PhysicsComponent>* getPhysicComponents();

	/**
	* Gets the vector containing all input components.
	* Gets the vector containing all input components. It
	* is used to loop over all input components during updates
	* as efficient as possible.
	*
	* @see createComponent()
	* @return Pointer to vector with all input components
	*/
	std::vector<InputComponent>* getInputComponents();

	/**
	 * Gets a component of a specified type for a specified entity.
	 * Gets a component of a specified type for a specified entity. The
	 * Component is returned as a pointer to a base component and must be
	 * cast to the right type.
	 *
	 * @see createComponent()
	 * @return Pointer to the component, must be cast from componentBase.
	 */
	ComponentBase* getComponent(componentTypes type, std::size_t entityID);

	//Helper functions
	/**
	 * Sets the position for a specified entity.
	 *
	 * @param Id for entity who's postions will be set.
	 * @param New position
	 * @see getEntityPos()
	 * @see createEntity()
	 */
	void setEntityPos(std::size_t id, glm::vec2& newPos);

	/**
	 * Gets the position of specified entity
	 *
	 * @param Id for entity
	 * @see setEntityPos()
	 * @see createEntity()
	 * @return Position of entity
	 */
	glm::vec2 getEntityPos(std::size_t& entityId);

	/**
	 * Sends a message to a specified component of an entity.
	 * Mostly used in components to communicate with eachother.
	 *
	 * @param Type of component to send message to
	 * @param Id of entity to message.
	 * @param Message
	 * @param Extra data if necessary
	 */
	void sendMessageToComponent(componentTypes type, std::size_t& entityId, char message[], void* data = nullptr);

	/**
	 * Saves all entities and components to file
	 */
	void writeToFile();

	/**
	 * Loads entitites and components from file
	 */
	void loadFromFile();

	/**
	 * Debug function.
	 * Prints out contents of all private vectors.
	 */
	void printAllVectorContents();

	/**
	* Gets the amount of entites existing
	*
	* @return Size of entities vector
	*/
	std::size_t getSize();
private:
	EntityComponentManager();

	std::size_t& translateID(std::size_t& entityId);

	std::vector<GameEntity> entities;
	std::vector<std::size_t> entityIdTranslationTable;
	std::vector<std::size_t> freeIdPool;

	std::vector<GraphicComponent> graphicsComponents;
	std::vector<InputComponent> inputComponents;
	std::vector<PhysicsComponent> physicsComponents;
	std::vector<PositionComponent> positionComponents;
	std::vector<PathingComponent> pathingComponents;

	const std::size_t invalidID = std::numeric_limits<std::size_t>::max();
};
