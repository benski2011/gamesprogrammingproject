#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
/**
* The different types of event that qui can send to the eventqueue
*/
class GUIEvent
{
public:
	enum types
	{
		BUILDING,
		SAVE,
		LOAD,
		QUIT,
		FOCUS,
		BACK,
		EXPLORE,
		ACTIVATEBUILDINGMENU,
		ASSIGNED_WORKER_NUMBER_CHANGED,
		NEW_PAWN,
		NUM_OF_EVENTS
	};

	struct Building
	{
		int id;
	};

	struct Save
	{
		int slot;
	};

	struct Load
	{
		int slot;
	};
	struct Focus
	{
		bool focus;
	};
	struct Explore
	{
	};
	struct ActivateBuldingMenu
	{
		bool active;
	};
	struct WorkerNumberChanged
	{
		unsigned int newAmount;
	};
	struct NewPawn
	{
	};
	types type;

	union
	{
		Building building;
		Save save;
		Load load;
		Focus focus;
		Explore explore;
		ActivateBuldingMenu active;
		WorkerNumberChanged worker;
		NewPawn newPawn;
	};

private:
};
