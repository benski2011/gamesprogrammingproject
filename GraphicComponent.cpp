#include "GraphicComponent.h"
#include "TextureHandler.h"

GraphicComponent::GraphicComponent(std::size_t entityId) : ComponentBase(entityId)
{
	mesh = meshManager::loadMesh("./spillprogrammering-gruppe-4-rimworld-2.0/models/knight/worker1.obj", 0.1);
}

GraphicComponent::GraphicComponent()
{
}

GraphicComponent::~GraphicComponent()
{
}

void GraphicComponent::recieveMessage(char message[], void * data)
{
	if (strcmp(message, "NewPosition") == 0)
	{
		modelMatrix = glm::translate(glm::mat4(), *(glm::vec3*)(data));
	}
	else if (strcmp(message, "Despawn") == 0)
	{
		visible = false;
	}
	else if (strcmp(message, "Spawn") == 0)
	{
		visible = true;
	}
}

void GraphicComponent::draw(GLuint shaderProgram)
{
	if (visible)
	{
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, false, glm::value_ptr(modelMatrix));

		meshManager::drawMesh(mesh);
	}
}