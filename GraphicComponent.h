#pragma once
#include <GL\glew.h>

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "EntityComponentManager.h"
#include "ComponentBase.h"
#include "MeshManager.h"

class GraphicComponent : public ComponentBase
{
public:
	/**
	 * @param Id for entity parent
	 */
	GraphicComponent(std::size_t entityId);

	GraphicComponent();
	~GraphicComponent();

	/**
	 * Recieves message.
	 * @param Message, wrong messages are ignored.
	 * @param Optional data.
	 */
	void recieveMessage(char message[], void* data);

	/**
	 * Draw call.
	 * @param Window to draw to.
	 */
	void draw(GLuint shaderProgram);

private:
	glm::mat4 modelMatrix;
	std::size_t mesh;

	GLuint vertexArrayObject, vertexBufferObject;
	bool visible = true;
};
