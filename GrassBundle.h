#pragma once

#include <GL\glew.h>
#include <vector>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include "TextureHandler.h"
#include "PerlinNoise.h"
#include "Shader.h"
#include "Map.h"
#include "Camera.h"

class Map;
enum terrain;

class GrassBundle
{
public:
	GrassBundle();
	/**
	* Creates a new bundle of grass covering a map, only
	* generates grass on Grass tiles
	*
	* @param map The terrain data
	* @param height The height data of the map
	* @param tileSize Size of the tiles
	*/
	GrassBundle(std::vector<std::vector<terrain>>& map, std::vector<std::vector<int>>& height, int tileSize);
	~GrassBundle();

	/**
	* Draws the grass
	*
	* @param shaderProgram The shader program used for drawing
	* @param camera The scenes camera
	* @param timeElapsed Time elapsed since the start of the program
	*/
	void draw(GLuint shaderProgram, Camera& camera, double timeElapsed);
private:
	GLuint grassTexture;
	GLuint vertexArrayObject;

	std::vector<glm::vec3> positions;
};
