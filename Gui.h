#pragma once

#include <vector>
#include <GL\glew.h>

#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include <SFGUI\RenderQueue.hpp>
#include <SFGUI/Renderers.hpp>

#include <SFML/Graphics.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/OpenGL.hpp>

#include "InformationTable.h"
#include "CityBuildingGui.h"
#include "WorldMapGui.h"
#include "EventQueue.h"
#include "GUIEvent.h"
#include "Menu.h"
#include "TradeGui.h"
#include "TradeGuiTest.h"
#include "Map.h"
#include "Trade.h"
#include "ResearchGUI.h"
#include "StartMenuGUI.h"
#include "PopUpGui.h"
/*
* The Different modes a Menu can be in
*/
enum Toggle
{
	WORLDMAP,			// The Gui is in the World Map mode
	TRADEMAP,			// The Gui is in trading mode
	CITYMAP,			// The Gui is in City building mode
	NumberOfToggle		// The Number of different modes
};

/*
* The gereral class for the GUI
*/
class Gui
{
public:
	/**
	* Constructor for the GUI
	* @param a pointer to The CivHandler
	* @param a Vector of BuildingType
	* @param a pointer to the players Resource class
	* @param a pointer to the trade class
	* @param a pointer to the ResearchHandler class
	*/
	Gui(CivHandler *civ, std::vector<Building>* buildings, Resources* resources, Trade* trade, ResearchHandler* res);
	/*
	* @return Desktop pointer
	*/
	sfg::Desktop* getDesktop();
	/*
	* @return SFGUI pointer
	*/
	sfg::SFGUI* getSFGUI();

	EventQueue<GUIEvent> events;
	/*
	* Toggles the Escape Menu
	*/
	void escManu();

	/*
	* Get what menu the gui is on
	* @return Toggle enum with the menu mode
	*/
	Toggle getToggle();

	/*
	* an Gui Update call for all gui elements
	*/
	void update(int pawns);
	/*
	* Activates the list of all buildings or removes the list and the information of one building
	* @param true if you want it to show false if you want to close it
	*/
	void displayBuildingMenu(bool active);
	/**
	*  removes information of the building type
	*/
	void removeBuildingInfo();
	/**
	* Acitvates a popup menu with the string text
	*/
	void activatePopUp(std::string text);
	/**
	* displays infomation about a built building.
	* @param the building that you want to display information about.
	*/
	void displayBuilding(building* b);
private:
	void toggleMap(Toggle clicked);
	sfg::Table::Ptr createmainMenu(Toggle map);

	sfg::SFGUI sfgui;
	sfg::Desktop desktop;
	sfg::Table::Ptr mainMenu[NumberOfToggle];
	Toggle toggle;

	InformationTable info;
	CityBuildingGui city;
	WorldMapGui world;
	PopUpGui popUp;

	TradeGuiTest tradeGuiTest;
	ResearchGUI research;

	Menu menu;
	StartMenuGUI startMenu;

	Trade* trade;
	Resources* resources;

	std::shared_ptr<sf::Font> font;
};
