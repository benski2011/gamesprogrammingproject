#include "InformationTable.h"

void InformationTable::update(int pawn)
{
	pawns->SetText("Pawns: " + std::to_string(pawn));
	for (size_t i = 0; i < res->NUM_OF_RESOURCES; i++)
	{
		values[i]->SetText(" " + std::to_string(res->getResource(Resources::Resource(i))));
	}
	values[res->NUM_OF_RESOURCES]->SetText(" " + std::to_string(res->getGold()));
	if (midWindow->IsGloballyVisible())
	{
		for (size_t i = 0; i < res->NUM_OF_TRADEGOODS; i++)
		{
			trade[i]->SetText(" " + std::to_string(res->getTradeGoods(Resources::TradeGoods(i))));
		}
	}
}

void InformationTable::showWindow(bool show)
{
	window->Show(show);
}

void InformationTable::setUpInfoWindow(Resources* res, ResearchGUI* research, EventQueue<GUIEvent>* eventQueue)
{
	this->res = res;
	counter = 0;
	window = sfg::Window::Create();
	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	window->SetAllocation(sf::FloatRect(1250, 0, 350, 300));
	window->SetRequisition(sf::Vector2f(350, 300));

	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto box2 = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	window->Add(box);
	table = sfg::Table::Create();

	for (size_t i = 0; i < res->NUM_OF_RESOURCES; i++)
	{
		sf::String temp = res->getResourceName()->at(i);
		createInfoLabel(temp, 10000);
	}
	createInfoLabel("Gold", res->getGold());
	pawns = sfg::Label::Create("pawn");
	sf::Image temp;
	sf::Image temp2;
	//table->Attach(lable2, sf::Rect<sf::Uint32>(1, 0, 2, 1), 3, 2, sf::Vector2f(0, 0));
	box->Pack(table);
	box->Pack(pawns);
	box->Pack(box2, false, false);

	temp.loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/data/Icon/infoPic.png");
	temp2.loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/data/Icon/Research.png");
	auto image = sfg::Image::Create(temp);
	auto image2 = sfg::Image::Create(temp2);
	auto button = sfg::Button::Create();
	auto button2 = sfg::Button::Create();
	button->SetImage(image);
	button2->SetImage(image2);

	button->SetAllocation(sf::FloatRect(1400, 250, 30, 30));
	button2->SetAllocation(sf::FloatRect(1500, 250, 30, 30));

	box2->Pack(button, false, false);
	box2->Pack(button2, false, false);
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([research, this]() {research->show(false); midWindow->Show(!(midWindow->IsGloballyVisible())); });
	button2->GetSignal(sfg::Widget::OnLeftClick).Connect([research, this]() { research->show(); midWindow->Show(false); });

	midWindow = sfg::Window::Create();
	midWindow->Show(false);

	midWindow->SetStyle(midWindow->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	midWindow->SetAllocation(sf::FloatRect(400, 100, 800, 600));
	midWindow->SetRequisition(sf::Vector2f(800, 600));
	auto midWindowBoxHorizontal = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto midWindowBoxVertical = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);

	auto scrollBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto tradeBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto trade = sfg::Label::Create("Trade Goods: ");
	auto pawnsBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto pawnsPrice = sfg::Label::Create("Pawns cost: 100 food");
	auto pawnButton = sfg::Button::Create("Hire Pawn");
	pawnButton->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue]()
	{
		GUIEvent event;
		event.type = GUIEvent::NEW_PAWN;
		eventQueue->pushEvent(event);
	});

	pawnsBox->Pack(pawnsPrice, false, false);
	pawnsBox->Pack(pawnButton, false, false);

	scrollBox->SetRequisition(sf::Vector2f(210, 200));
	midWindowBoxVertical->SetRequisition(sf::Vector2f(500, 550));
	midWindowBoxHorizontal->Pack(midWindowBoxVertical, false, false);
	midWindowBoxVertical->Pack(scrollBox, false, false);
	midWindowBoxVertical->Pack(pawnsBox, false, false);
	midWindow->Add(midWindowBoxHorizontal);

	scrollBox->Pack(trade, true, true);
	auto scroll = sfg::ScrolledWindow::Create();
	scroll->SetRequisition(sf::Vector2f(150, 500));
	scroll->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_NEVER);
	scroll->SetPlacement(sfg::ScrolledWindow::Placement::TOP_RIGHT);
	scroll->AddWithViewport(tradeBox);

	scrollBox->Pack(scroll, true, true);
	for (size_t i = 0; i < Resources::NUM_OF_TRADEGOODS; i++)
	{
		createLabel(res->gettradeGoodsName()->at(i), tradeBox);
	}
	auto close = sfg::Button::Create("Close");
	close->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {midWindow->Show(false); });
	scrollBox->Pack(close, false, true);
}

void InformationTable::addWindows(sfg::Desktop * desktop)
{
	desktop->Add(window);
	desktop->Add(midWindow);
}

void InformationTable::createInfoLabel(sf::String text, int value)
{
	auto box = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	sf::Image temp;
	if (temp.loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/data/Icon/" + text + ".png"))
	{
		auto image = sfg::Image::Create(temp);
		box->Pack(image, false, false);
	}
	else
	{
		auto name = sfg::Label::Create(text);
		box->Pack(name, false, false);
	}

	auto amount = sfg::Label::Create(" " + std::to_string(value));
	box->Pack(amount, true, true);
	box->SetRequisition(sf::Vector2f(280 / 3, 0));
	auto frame = sfg::Frame::Create();

	frame->Add(box);

	table->Attach(frame, sf::Rect<sf::Uint32>(counter % 3, counter / 3, 1, 1), 0, 2, sf::Vector2f(1, 0));
	counter++;
	values.push_back(amount);
}

void InformationTable::createLabel(sf::String text, sfg::Box::Ptr box)
{
	auto box2 = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	//sf::Image temp;
	//if (temp.loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/data/Icon/" + text + ".png"))
	//{
	//	auto image = sfg::Image::Create(temp);
	//	box2->Pack(image, false, false);
	//}
	//else
	{
		auto name = sfg::Label::Create(text);
		box2->Pack(name, false, false);
	}

	auto amount = sfg::Label::Create(" " + std::to_string(10000));
	box2->Pack(amount, true, true);
	box2->SetRequisition(sf::Vector2f(150, 0));

	auto frame = sfg::Frame::Create();
	frame->Add(box2);

	box->Pack(frame, true, true);
	trade.push_back(amount);
}