#pragma once
#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>
#include <string>
#include "Resources.h"
#include "ResearchGUI.h"
#include "EventQueue.h"
#include "GUIEvent.h"

class InformationTable
{
public:
	/*
	* Sets up the InformationTableWindow
	* @param a pointer to the players Resource class
	* @param pointer to the ResearchHandler class
	*/
	void setUpInfoWindow(Resources* res, ResearchGUI* research, EventQueue<GUIEvent>* eventQueue);
	/*
	* Updates the InformationTable
	*/
	void update(int pawns);
	/*
	* Sets the visability of the InformationTable based on param
	* @param true if the InformationTable should be seen false if not
	*/
	void showWindow(bool show);
	/*
	* Adds the InformationTable to Desktop
	* @param the sfg::Desktop you want to add it too
	*/
	void addWindows(sfg::Desktop* desktop);

private:

	void createInfoLabel(sf::String text, int value);
	void createLabel(sf::String text, sfg::Box::Ptr box);

	sfg::Window::Ptr window;
	sfg::Window::Ptr midWindow;

	sfg::Table::Ptr table;

	int counter;

	Resources* res;

	std::vector<sfg::Label::Ptr> values;
	std::vector<sfg::Label::Ptr> trade;
	sfg::Label::Ptr pawns;
};
