#pragma once

#include <SFML\Graphics\Vertex.hpp>

#include "ComponentBase.h"
#include "EntityComponentManager.h"

class InputComponent : public ComponentBase
{
public:
	/**
	* @param Id for entity parent
	*/
	InputComponent(std::size_t& entityId);
	InputComponent();
	~InputComponent();

	/**
	* Recieves a message and processes it
	*
	* @param message The message
	* @param data Optional data paremeter
	*/
	void recieveMessage(char message[], void* data);

	/**
	* Starts moving the entity towards a specified point. Uses AStar to
	* calculate the path.
	*
	* @param pos Position to move to
	* @param deSpawn Flag if the entity will despawn at the end of the movement or not
	*/
	void moveHere(glm::vec2& pos, bool deSpawn = false);
private:
	bool despawnFlag;
};