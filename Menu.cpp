#include "Menu.h"

bool Menu::toggleMenu()
{
	activated = !activated;
	window->Show(activated);
	return activated;
}

void Menu::AddToDesktop(sfg::Desktop * desktop)
{
	desktop->Add(window);
}

void Menu::setUpMenu(EventQueue<GUIEvent>* eventQueue)
{
	activated = false;
	window = sfg::Window::Create();
	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR);
	window->Show(activated);

	auto save = sfg::Button::Create("Save");
	auto load = sfg::Button::Create("Load");
	auto quit = sfg::Button::Create("Quit");
	auto back = sfg::Button::Create("Back");
	save->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::SAVE;
		event.save.slot = 0;
		eventQueue->pushEvent(event); });

	load->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::LOAD;
		event.load.slot = 0;
		eventQueue->pushEvent(event); });
	quit->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::QUIT;
		eventQueue->pushEvent(event); });
	back->GetSignal(sfg::Widget::OnLeftClick).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::BACK;
		eventQueue->pushEvent(event); });

	auto table = sfg::Table::Create();
	table->Attach(back, sf::Rect<sf::Uint32>(0, 0, 1, 1));
	table->Attach(save, sf::Rect<sf::Uint32>(0, 1, 1, 1));
	table->Attach(load, sf::Rect<sf::Uint32>(0, 3, 1, 1));
	table->Attach(quit, sf::Rect<sf::Uint32>(0, 4, 1, 1));
	table->SetAllocation(sf::FloatRect(0, 0, 300, 300));

	window->Add(table);

	window->GetSignal(sfg::Widget::OnMouseEnter).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::FOCUS;
		event.focus.focus = true;
		eventQueue->pushEvent(event); });

	window->GetSignal(sfg::Widget::OnMouseLeave).Connect([eventQueue] {
		GUIEvent event;
		event.type = GUIEvent::FOCUS;
		event.focus.focus = false;
		eventQueue->pushEvent(event); });

	sf::Vector2f pos = sf::Vector2f(400, 300);

	window->SetAllocation(sf::FloatRect(1600 / 2 - pos.x / 2, 900 / 2 - pos.y / 2, pos.x, pos.y)); // should be changed to scale
}