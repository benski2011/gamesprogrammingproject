#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include <SFGUI\RenderQueue.hpp>

#include "EventQueue.h"
#include "GUIEvent.h"

class Menu
{
public:
	/*
	* Sets Up the Esc menu
	* @param The GUi event queue
	*/
	void setUpMenu(EventQueue<GUIEvent>* eventQueue);
	/*
	* show/hides the menu;
	*/
	bool toggleMenu();
	/*
	* Adds the Menu To the Desktop
	* @param the sfg::Desktop you want to connect too
	*/
	void AddToDesktop(sfg::Desktop* desktop);
private:

	bool activated;
	sfg::Window::Ptr window;
};
