#include "MeshManager.h"

std::vector<Mesh> meshes;

std::size_t meshManager::loadMesh(std::string filePath, double scaling)
{
	for (std::size_t i = 0; i < meshes.size(); ++i)
	{
		if (!filePath.compare(meshes[i].path))
		{
			return i;
		}
	}

	meshes.push_back(Mesh(filePath, scaling));

	return meshes.size() - 1;
}

void meshManager::drawMesh(std::size_t index)
{
	assert(meshes.size() > index);

	meshes[index].draw();
}