#pragma once

#include "Mesh.h"

#include <vector>
#include <string>
#include <cassert>

namespace meshManager
{
	/**
	* Loads a mesh from a file.
	*
	* @param filePath Filepath to load model from
	* @param scaling Scale to scale model with
	*/
	std::size_t loadMesh(std::string filePath, double scaling = 5.0);

	/**
	* Draws the mesh.
	*
	* @param index Which mesh to draw
	*/
	void drawMesh(std::size_t index);
}
