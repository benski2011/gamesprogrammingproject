#pragma once

#include <SFML\Graphics\Vertex.hpp>
#include <vector>

#include "ComponentBase.h"
#include "AStar.h"

class PathingComponent : public ComponentBase
{
	friend class EntityComponentManager;
public:
	/**
	* Constructs a new PathingComponent
	*
	* @param entityId Parent id
	*/
	PathingComponent(std::size_t entityId);
	PathingComponent();
	~PathingComponent();

	/**
	* Recieves a message and processes it
	*
	* @param message The message
	* @param data Optional data paremeter
	*/
	void recieveMessage(char message[], void* data);

private:
	int pathIndex;
	std::vector<glm::vec2> path;
};
