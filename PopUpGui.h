#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include <string>

class PopUpGui
{
public:
	/*
	* Sets up the InformationTableWindow
	* @param a pointer to the players Resource class
	* @param pointer to civHandler
	* @param pointer the trade class
	* @param a pointer to the GUI EventQueue
	*/
	void SetUp();
	/*
	* Adds the TradeGui to Desktop
	* @param the sfg::Desktop you want to add it too
	*/
	void addToDesktop(sfg::Desktop *desktop);
	/*
	* Shows the TradeGui based on a bool
	* @param true = show
	*/
	void activatePopUp(bool active, std::string msg);
	/*
	* Updates the TradeGui
	*/
	void update();
private:
	sfg::Window::Ptr window;
	sfg::Desktop* desktop;
	sfg::Label::Ptr text;
};
