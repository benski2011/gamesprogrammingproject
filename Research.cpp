#include "Research.h"

Research::Research(int id, sf::String tittle, sf::String text, int time)
{
	this->id = id;
	this->tittle = tittle;
	this->text = text;
	finished = false;
	this->time = time;
	maxTime = time;
	level = -1;
}

bool Research::isFinished()
{
	return finished;
}

std::string Research::getName()
{
	return tittle;
}

std::string Research::getText()
{
	return text;
}

void Research::addreqid(int id)
{
	req.push_back(id);
}

std::vector<int> Research::getReq()
{
	return req;
}

bool Research::updateTime(int i)
{
	time += i;
	if (time <= 0)
	{
		finished = true;
	}
	return finished;
}

void Research::setLevel(int level)
{
	this->level = level;
}

int Research::getLevel()
{
	return level;
}

int Research::getId()
{
	return id;
}