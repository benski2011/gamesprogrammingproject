#include "ResearchGUI.h"

void ResearchGUI::setUp(ResearchHandler* res)
{
	window = sfg::Window::Create();
	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	window->SetAllocation(sf::FloatRect(300, 50, 800, 600));
	window->SetRequisition(sf::Vector2f(945, 800));

	this->res = res;
	id = -1;
	auto box = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto researchScreen = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto infoBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	infoBox->SetAllocation(sf::FloatRect(300, 650, 0, 0));
	infoBox->SetRequisition(sf::Vector2f(0, 200));

	box->Pack(researchScreen);
	box->Pack(infoBox, false, false);
	auto frame = sfg::Frame::Create("Research");
	researchScreen->Pack(frame);
	infoFrame = sfg::Frame::Create("Information:");
	infoBox->Pack(infoFrame);

	//researchScreen

	auto scrollBox = sfg::Box::Create();
	auto ScrollTable = sfg::ScrolledWindow::Create();
	ScrollTable->SetRequisition(sf::Vector2f(0, 0));
	ScrollTable->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_ALWAYS);
	ScrollTable->SetPlacement(sfg::ScrolledWindow::Placement::TOP_LEFT);
	ScrollTable->AddWithViewport(scrollBox);

	auto table = sfg::Table::Create();
	scrollBox->Pack(table);

	frame->Add(ScrollTable);
	auto research = res->getResearch();
	int f = -1, level = 0;
	std::vector<int> NrLevel;
	for (size_t i = 0; i < research->size(); i++)
	{
		level = research->at(i).getLevel();
		if (NrLevel.size() <= level)
		{
			while (NrLevel.size() <= level)
			{
				int temp = 0;
				NrLevel.push_back(temp);
			}
		}
		NrLevel[level]++;

		createButton(research->at(i).getId(), research->at(i).getName(), level, table, NrLevel.at(level));
	}

	// Information box

	researchButton = sfg::Button::Create("Research");

	infoBox->Pack(researchButton, false, false);
	researchButton->GetSignal(sfg::Widget::OnLeftClick).Connect([this, res] {res->setActiveResearch(id); });
	auto moreInfo = sfg::Box::Create();
	text = sfg::Label::Create("Choose a Research topic!");
	moreInfo->Pack(text);
	infoFrame->Add(moreInfo);
	anotherBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	anotherBox->SetRequisition(sf::Vector2f(200, 0));
	moreInfo->Pack(anotherBox, false, false);
	auto req = sfg::Label::Create("Requirments: ");
	req->SetId("req");
	req->SetAllocation(sf::FloatRect(900, 650, 40, 0));
	req->SetRequisition(sf::Vector2f(0, 40));
	anotherBox->Pack(req, false, false);

	text->SetId("text");
	infoFrame->Show(false);
	researchButton->Show(false);
	window->Add(box);
	window->Show(false);
}

void ResearchGUI::show(bool show)
{
	window->Show(show);
}

void ResearchGUI::show()
{
	window->Show(!window->IsGloballyVisible());
}

void ResearchGUI::setDesktop(sfg::Desktop * desktop)
{
	desktop->Add(window);
	this->desktop = desktop;
}

void ResearchGUI::update()
{
	if (window->IsGloballyVisible() && res->getChanged())
	{
		auto temp = res->getResearch();
		std::string prop;
		for (size_t i = 0; i < temp->size(); i++)
		{
			prop.append("Button#Research" + std::to_string(i) + ":Normal {\n");
			prop.append("		BackgroundColor: ");
			if (temp->at(i).isFinished())
			{
				prop.append("#449442FF;");
			}
			else if (!res->canBeResearched(i))
			{
				prop.append("#A2A2A2FF;");
			}
			else
			{
				prop.append("#000000ff;");
			}
			prop.append("}\n");
		}
		desktop->SetProperties(prop);
		res->setChanged(false);
	}
}

void ResearchGUI::createButton(int id, sf::String name, int level, sfg::Table::Ptr table, int nr)
{
	//sf::Image temp;
	auto button = sfg::Button::Create();
	//if (temp.loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/data/Icon/" + name + ".png"))
	//{
	//	auto image = sfg::Image::Create(temp);
	//	button->SetImage(image);
	//}
	//else
	{
		button->SetLabel(name);
	}

	table->Attach(button, sf::Rect<sf::Uint32>(nr, level, 1, 1), 3, 2, sf::Vector2f(0, 50));
	table->SetAllocation(sf::FloatRect(0, 0, 50, 50));
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([id, this] {switchInfo(id); });
	button->SetId("Research" + std::to_string(id));
}

void ResearchGUI::switchInfo(int id)
{
	std::string prop("");
	infoFrame->Show(true);
	researchButton->Show(true);
	Research temp = res->getResearch()->at(id);
	bool canBeResearched = res->canBeResearched(id);
	bool Researched = temp.isFinished();
	infoFrame->SetLabel(temp.getName());
	this->id = id;
	auto nr = temp.getReq();
	for (size_t i = 0; i < nr.size(); i++)
	{
		if (i >= labels.size())
		{
			auto label = sfg::Label::Create();
			labels.push_back(label);
			label->SetId("req" + std::to_string(i));
			anotherBox->Pack(label);
		}
		labels[i]->SetText((res->getResearch())->at(nr.at(i)).getName());
		prop.append("Label#req" + std::to_string(i) + ":Normal {\n");
		prop.append("		Color: ");
		if (res->getResearch()->at(nr.at(i)).isFinished())
		{
			prop.append("#00FF00FF;");
		}
		else
		{
			prop.append("#FF0000FF;");
		}

		prop.append("}\n");
	}
	for (size_t i = nr.size(); i < labels.size(); i++)
	{
		labels[i]->SetText("");
	}
	prop.append("Label#req:Normal {\n");
	prop.append("		Color: ");
	if (Researched || !canBeResearched)
	{
		researchButton->SetState(sfg::Widget::State::INSENSITIVE);
		if (!canBeResearched)
		{
			prop.append("#FF0000FF;");
		}
	}
	else
	{
		prop.append("#00FF00FF;");
		researchButton->SetState(sfg::Widget::State::NORMAL);
	}
	prop.append("}\n");
	if (!desktop->SetProperties(prop))
	{
		printf("FAILED!\n");
	}
	text->SetText(temp.getText());
}