#pragma once
#include <SFGUI\Widgets.hpp>
#include <SFGUI/SFGUI.hpp>

#include "ResearchHandler.h"

class ResearchGUI
{
public:
	/*
	* Sets up the ResearchGUI
	* @param the researchHandler used
	*/
	void setUp(ResearchHandler* res);
	/*
	* Shows the ResearchGUI based on a bool
	* @param true = show
	*/
	void show(bool show);
	/*
	* Shows the ResearchGUI based on a toggle
	*/
	void show();
	/*
	* Adds the ResearchGUI To the Desktop
	* @param the sfg::Desktop you want to connect too
	*/
	void setDesktop(sfg::Desktop* desktop);
	/*
	* Updates the ResearchGUI
	*/
	void update();
private:
	void createButton(int id, sf::String name, int pos, sfg::Table::Ptr table, int nr);
	void switchInfo(int id);

	sfg::Desktop* desktop;

	sfg::Window::Ptr window;
	sfg::Frame::Ptr infoFrame;
	sfg::Box::Ptr anotherBox;
	sfg::Button::Ptr researchButton;
	sfg::Label::Ptr text;
	ResearchHandler* res;

	std::vector<sfg::Label::Ptr> labels;

	int id;
};
