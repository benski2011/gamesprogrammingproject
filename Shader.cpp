#include "Shader.h"

Shader::Shader(std::string fileName, bool geometryShader)
{
	std::string vertexPath = fileName + ".vs";
	std::string fragmentPath = fileName + ".fs";
	std::string geometryPath = fileName + ".gs";

	std::string vertexCode;
	std::string fragmentCode;
	std::string geometryCode;
	std::ifstream vShaderFile;
	std::ifstream fShaderFile;
	std::ifstream gShaderFile;

	vShaderFile.exceptions(std::ifstream::badbit);
	fShaderFile.exceptions(std::ifstream::badbit);
	gShaderFile.exceptions(std::ifstream::badbit);

	try
	{
		vShaderFile.open(vertexPath);
		fShaderFile.open(fragmentPath);
		if (geometryShader)
		{
			gShaderFile.open(geometryPath);
		}

		std::stringstream vShaderStream, fShaderStream, gShaderStream;

		vShaderStream << vShaderFile.rdbuf();
		fShaderStream << fShaderFile.rdbuf();

		if (geometryShader)
		{
			gShaderStream << gShaderFile.rdbuf();
			gShaderFile.close();
		}

		vShaderFile.close();
		fShaderFile.close();
		vertexCode = vShaderStream.str();
		fragmentCode = fShaderStream.str();
		if (geometryShader)
		{
			geometryCode = gShaderStream.str();
		}
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "Shader error. File not properly read.\n";
	}

	const GLchar* vShaderCode = vertexCode.c_str();
	const GLchar* fShaderCode = fragmentCode.c_str();
	const GLchar* gShaderCode;
	if (geometryShader)
	{
		gShaderCode = geometryCode.c_str();
	}

	GLuint vertex, fragment, geometry;
	GLint success;
	GLchar infoLog[512];

	vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex, 1, &vShaderCode, NULL);
	glCompileShader(vertex);

	glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertex, 512, NULL, infoLog);
		std::cout << "Vertex Shader failed to compile.\nInfoLog: " << infoLog << std::endl;
	}

	fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment, 1, &fShaderCode, NULL);
	glCompileShader(fragment);

	glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragment, 512, NULL, infoLog);
		std::cout << "Fragment Shader failed to compile.\nInfoLog: " << infoLog << std::endl;
	}

	if (geometryShader)
	{
		geometry = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometry, 1, &gShaderCode, NULL);
		glCompileShader(geometry);

		glGetShaderiv(geometry, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(geometry, 512, NULL, infoLog);
			std::cout << "Geometry Shader failed to compile.\nInfoLog: " << infoLog << std::endl;
		}
	}

	this->program = glCreateProgram();
	glAttachShader(this->program, vertex);
	glAttachShader(this->program, fragment);
	if (geometryShader)
	{
		glAttachShader(this->program, geometry);
	}
	glLinkProgram(this->program);

	glGetProgramiv(this->program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(program, 512, NULL, infoLog);
		std::cout << "Program failed to link.\nInfoLog;: " << infoLog << std::endl;
	}

	glDeleteShader(fragment);
	glDeleteShader(vertex);
	if (geometryShader)
	{
		glDeleteShader(geometry);
	}
}

void Shader::bind()
{
	glUseProgram(program);
}