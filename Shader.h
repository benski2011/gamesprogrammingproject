#pragma once

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <GL\glew.h>

class Shader
{
public:
	/**
	* Loads a shader from file.
	*
	* @param filePath File path to load shader from
	* @param geometryShader If there is a geometry shader or not
	*/
	Shader(std::string filePath, bool geometryShader = false);

	/**
	* Bind the shader
	*/
	void bind();

	/**
	* Gets the shader program.
	*
	* @return The shader program
	*/
	GLuint getProgram()
	{
		return program;
	}

private:
	GLuint program;
};