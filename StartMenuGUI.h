#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>

#include "EventQueue.h"
#include "GUIEvent.h"

#define buttonSize 25
/**
* this is a class for the start menu. it is as of 17/12/2017 not used.
*/
class StartMenuGUI
{
public:
	/**
	* Sets up the start menu
	* @param the event system to gui
	*/
	void setUp(EventQueue<GUIEvent>* eventQueue);
	/**
	* activates the menu or close it
	* @ param show
	*/
	void activateStartMenuGUI(bool active);
	/**
	* adds the StartMenu to the desktop
	* @ the desktop you want to connect too.
	*/
	void AddToDesktop(sfg::Desktop* desktop);
private:

	sfg::Window::Ptr window;
};
