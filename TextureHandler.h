#pragma once

#include <vector>
#include <GL\glew.h>
#include <string>

#include "stb_image.h"

namespace textureHandler
{
	/**
	* Loads a texture from file
	*
	* @param filePath File path to load from
	*/
	GLuint loadFromFile(std::string filePath);

	/**
	* Binds the texture.
	*
	* @param texture Texture to bind
	* @param type Type of texture to bind
	* @param slot Texture slot to bind to
	*/
	void bindTexture(GLuint texture, int type, int slot);
}
