#include "Trade.h"

Trade::Trade(Resources* resources, CivHandler* civHandler)
{
	difficulty = 1;
	playerResources = resources;
	this->civHandler = civHandler;
}

bool Trade::doTrade(std::vector<TradeSum> player, std::vector<TradeSum> civ, int civNr)
{
	std::string string;
	bool willTrade = civHandler->getcivs()->at(civNr).willTrade(player, civ, string);
	if (willTrade)
	{
		Resources* temp = civHandler->getcivs()->at(civNr).getResource();
		for (size_t i = 0; i < player.size(); i++)
		{
			if (player[i].type == 0)
			{
				playerResources->addResource((Resources::Resource)player[i].typeList, -player[i].amount);
				temp->addResource((Resources::Resource)player[i].typeList, player[i].amount);
			}
			else if (player[i].type == 2)
			{
				playerResources->addGold(-player[i].amount);
				temp->addGold(player[i].amount);
			}
			else
			{
				playerResources->addTradeGood((Resources::TradeGoods)player[i].typeList, +player[i].amount);
				temp->addTradeGood((Resources::TradeGoods)player[i].typeList, player[i].amount);
			}
		}
		for (size_t i = 0; i < civ.size(); i++)
		{
			if (civ[i].type == 0)
			{
				playerResources->addResource((Resources::Resource)civ[i].typeList, civ[i].amount);
				temp->addResource((Resources::Resource)civ[i].typeList, -civ[i].amount);
			}
			else if (civ[i].type == 2)
			{
				playerResources->addGold(civ[i].amount);
				temp->addGold(-civ[i].amount);
			}
			else
			{
				playerResources->addTradeGood((Resources::TradeGoods)civ[i].typeList, civ[i].amount);
				temp->addTradeGood((Resources::TradeGoods)civ[i].typeList, -civ[i].amount);
			}
		}
	}
	return willTrade;
}

int Trade::getCost(bool tradegoods, int type, bool add, int civNr)
{
	return cost(tradegoods, type, 1, add, civNr);
}

int Trade::cost(bool tradegoods, int type, int amount, bool buy, int civNr)
{
	return 300 * amount;
}