#pragma once
#include <string>

#include "Resources.h"
#include "CivHandler.h"

class CivHandler;

struct TradeSum
{
	bool used = false;
	std::string name;
	int type;
	int typeList;
	int amount;
	sfg::Label::Ptr label;
	sfg::Box::Ptr box;
	sfg::CheckButton::Ptr check;
};

class Trade
{
public:
	/**
	* sets up the trade class
	* @param the players resources class
	* @param the civhandler
	*/
	Trade(Resources* resources, CivHandler* civHandler);
	/*
	* get the cost of a resource or a tradegood from a civ
	* @param true if its a tradegood, false if its a resource
	* @param the type of resource/tradegoods
	* @param if your buying or selling
	* @param which civ you want to trade with
	* @return the calculated cost of the trade
	*/
	int getCost(bool tradegoods, int type, bool add, int civNr);
	/**
	* does a trade with a civ
	* @param the players traded items
	* @param the civs traded items
	* @param id of the civ chosen
	*/
	bool doTrade(std::vector<TradeSum> player, std::vector<TradeSum> civ, int civNr);
private:
	int cost(bool tradegoods, int type, int amount, bool add, int civNr);
	int difficulty;
	Resources* playerResources;
	CivHandler* civHandler;
};
