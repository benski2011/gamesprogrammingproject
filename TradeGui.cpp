#include "TradeGui.h"

void TradeGui::tradeSetUp(Resources* res, CivHandler* c, Trade* trade, EventQueue<GUIEvent>* eventQueue)
{
	resources = res;
	civ = c;
	type = 0;
	civNumber = 0;
	Tradegoods = false;
	civChosen = false;
	this->trade = trade;
	this->eventQueue = eventQueue;
	// Create all widgets
	window = sfg::Window::Create();
	//Crated boxes
	auto mainBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	auto resourcesBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto midBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto rightBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	// Create Resource widgets
	auto resourceName = sfg::Label::Create("Resource: ");
	resourceName->SetRequisition(sf::Vector2f(300, 0));

	auto tradeGoodsName = sfg::Label::Create("TradeGoods: ");
	tradeGoodsName->SetRequisition(sf::Vector2f(300, 0));

	civName = sfg::Label::Create("Civilization: ");
	civName->SetRequisition(sf::Vector2f(300, 0));

	auto resourceBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto tradeGoodsBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto civBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	std::vector<sf::String>* temp = resources->getResourceName();
	for (size_t i = 0; i < resources->NUM_OF_RESOURCES; i++)
	{
		createResourceButton(temp->at(i), resourceBox, i);
	}
	temp = resources->gettradeGoodsName();
	for (size_t i = 0; i < resources->NUM_OF_TRADEGOODS; i++)
	{
		createTradeGoodsButton(temp->at(i), tradeGoodsBox, i);
	}

	std::vector<std::string> civNames = civ->getNames();
	for (size_t i = 0; i < civNames.size(); i++)
	{
		sf::String temp = civNames[i];
		createCivButton(temp, civBox, i);
	}

	auto resourceScrollTable = sfg::ScrolledWindow::Create();
	resourceScrollTable->SetRequisition(sf::Vector2f(.0f, 400.f));
	resourceScrollTable->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_NEVER);
	resourceScrollTable->SetPlacement(sfg::ScrolledWindow::Placement::TOP_RIGHT);
	resourceScrollTable->AddWithViewport(resourceBox);

	auto tradeGoodsScrollTable = sfg::ScrolledWindow::Create();
	tradeGoodsScrollTable->SetRequisition(sf::Vector2f(.0f, 400.f));
	tradeGoodsScrollTable->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_NEVER);
	tradeGoodsScrollTable->SetPlacement(sfg::ScrolledWindow::Placement::TOP_RIGHT);
	tradeGoodsScrollTable->AddWithViewport(tradeGoodsBox);

	civScrollBox = sfg::ScrolledWindow::Create();
	civScrollBox->SetRequisition(sf::Vector2f(.0f, 800.f));
	civScrollBox->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_ALWAYS | sfg::ScrolledWindow::HORIZONTAL_NEVER);
	civScrollBox->SetPlacement(sfg::ScrolledWindow::Placement::TOP_LEFT);
	civScrollBox->AddWithViewport(civBox);

	auto middleTable = sfg::Table::Create();
	TopLabel = sfg::Label::Create("Resource");
	yourValue = sfg::Label::Create("You: 200");
	theirValue = sfg::Label::Create("Their: 200");
	yourGold = sfg::Label::Create("Gold: 200");
	cost = sfg::Label::Create("Cost: " + std::to_string(trade->getCost(Tradegoods, type, true, civNumber)));

	entry = sfg::Entry::Create();
	auto buy = sfg::Button::Create("Buy");
	auto sell = sfg::Button::Create("Sell");
	Resources* tempResourcesPointer = resources;
	buy->GetSignal(sfg::Widget::OnLeftClick).Connect([this] { this->doTrade(entry->GetText(), true); });
	sell->GetSignal(sfg::Widget::OnLeftClick).Connect([this] { this->doTrade(entry->GetText(), false);  });

	middleTable->Attach(TopLabel, sf::Rect<sf::Uint32>(0, 0, 2, 1), 3, 2, sf::Vector2f(0, 20));
	middleTable->Attach(yourGold, sf::Rect<sf::Uint32>(0, 1, 1, 1), 3, 2, sf::Vector2f(0, 20));
	middleTable->Attach(cost, sf::Rect<sf::Uint32>(1, 1, 1, 1), 3, 2, sf::Vector2f(0, 20));
	middleTable->Attach(yourValue, sf::Rect<sf::Uint32>(0, 2, 1, 1), 3, 2, sf::Vector2f(0, 50));
	middleTable->Attach(theirValue, sf::Rect<sf::Uint32>(1, 2, 1, 1), 3, 2, sf::Vector2f(0, 50));
	middleTable->Attach(entry, sf::Rect<sf::Uint32>(0, 3, 2, 1), 3, 2, sf::Vector2f(0, 20));
	middleTable->Attach(buy, sf::Rect<sf::Uint32>(0, 4, 1, 1), 3, 2, sf::Vector2f(0, 20));
	middleTable->Attach(sell, sf::Rect<sf::Uint32>(1, 4, 1, 1), 3, 2, sf::Vector2f(0, 20));
	middleTable->SetAllocation(sf::FloatRect(0, 0, 200, 100));

	civInfoBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto civTable = sfg::Table::Create();

	civGold = sfg::Label::Create("gold");
	auto civOpinion = sfg::Label::Create("Opinion: 50");
	auto civTime = sfg::Label::Create("TravelTime: ???");
	civInfo = sfg::Label::Create("temp");

	civTable->Attach(civInfo, sf::Rect<sf::Uint32>(0, 0, 2, 1), 3, 2, sf::Vector2f(0, 20));
	civTable->Attach(civOpinion, sf::Rect<sf::Uint32>(0, 1, 2, 1), 3, 2, sf::Vector2f(0, 20));
	civTable->Attach(civGold, sf::Rect<sf::Uint32>(0, 2, 2, 1), 3, 2, sf::Vector2f(0, 20));
	civTable->Attach(civTime, sf::Rect<sf::Uint32>(0, 3, 2, 1), 3, 2, sf::Vector2f(0, 20));
	civTable->SetAllocation(sf::FloatRect(0, 0, 200, 100));

	auto back = sfg::Button::Create("Back");
	back->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {
		civScrollBox->Show(true);
		civInfoBox->Show(false);
	});

	sf::Image temp23;
	temp23.loadFromFile("./spillprogrammering-gruppe-4-rimworld-2.0/data/Lord.jpg");
	civImage = sfg::Image::Create();
	civImage->SetImage(temp23);
	civInfoBox->Pack(civImage, false);
	civInfoBox->Pack(civTable, false, false);
	civInfoBox->Pack(back, false, false);
	window->Add(mainBox);

	mainBox->Pack(resourcesBox, false, false);
	mainBox->Pack(midBox);
	mainBox->Pack(rightBox, false, false);

	midBox->Pack(middleTable);

	resourcesBox->Pack(resourceName, false);
	resourcesBox->Pack(resourceScrollTable, false);

	resourcesBox->Pack(tradeGoodsName, false);
	resourcesBox->Pack(tradeGoodsScrollTable);

	rightBox->Pack(civName, false);
	rightBox->Pack(civScrollBox);
	rightBox->Pack(civInfoBox);
	civInfoBox->Show(false);

	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR);

	resourcesBox->SetSpacing(5.0f);
	midBox->SetSpacing(50.0f);
	rightBox->SetSpacing(5.0f);
	window->SetAllocation(sf::FloatRect(275, 0, 1325, 900));
	resourcesBox->SetAllocation(sf::FloatRect(300, 0, 300, 900));
	midBox->SetAllocation(sf::FloatRect(575, 0, 700, 900));
	rightBox->SetAllocation(sf::FloatRect(1325, 0, 300, 900));

	changeGoods(Tradegoods, type);

	textWindow = sfg::Window::Create(sfg::Window::BACKGROUND);

	sf::Vector2f pos = sf::Vector2f(400, 300);
	text = sfg::Label::Create("");
	auto textBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	auto close = sfg::Button::Create("Close");
	close->GetSignal(sfg::Widget::OnLeftClick).Connect([this] {
		textWindow->Show(false);
	});

	textBox->Pack(text);
	textBox->Pack(close, false, false);
	textWindow->Add(textBox);
	textWindow->SetAllocation(sf::FloatRect(1600 / 2 - pos.x / 2, 900 / 2 - pos.y / 2, pos.x, pos.y));
	textWindow->Show(false);
}

void TradeGui::createResourceButton(sf::String name, sfg::Box::Ptr box, int type)
{
	auto button = sfg::Button::Create(name);
	int i = 0;
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([i, type, this]() {this->changeGoods(i, type); });
	button->SetRequisition(sf::Vector2f(250, 50));
	box->Pack(button);
}

void TradeGui::createTradeGoodsButton(sf::String name, sfg::Box::Ptr box, int type)
{
	auto button = sfg::Button::Create(name);
	int i = 1;
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([i, type, this]() {this->changeGoods(i, type); });
	button->SetRequisition(sf::Vector2f(250, 50));
	box->Pack(button);
}

void TradeGui::createCivButton(sf::String name, sfg::Box::Ptr box, int civNr)
{
	auto button = sfg::Button::Create(name);

	button->GetSignal(sfg::Widget::OnLeftClick).Connect([civNr, this]() {this->changeCiv(civNr); });
	button->SetRequisition(sf::Vector2f(250, 50));
	box->Pack(button);
}

void TradeGui::changeGoods(int check, int type)
{
	std::vector<sf::String>* temp;
	Tradegoods = check;
	this->type = type;
	if (!Tradegoods)
	{
		temp = resources->getResourceName();
		TopLabel->SetText(temp->at(type));
		yourValue->SetText("You: " + std::to_string(resources->getResource((Resources::Resource)(type))));
		theirValue->SetText("Their: " + std::to_string(civ->getcivs()->at(civNr).getResource()->getResource((Resources::Resource)(type))));
	}
	else
	{
		temp = resources->gettradeGoodsName();
		TopLabel->SetText(temp->at(type));
		yourValue->SetText("You: " + std::to_string(resources->getTradeGoods((Resources::TradeGoods)(type))));
		theirValue->SetText("Their: " + std::to_string(civ->getcivs()->at(civNr).getResource()->getTradeGoods((Resources::TradeGoods)(type))));
	}
	yourGold->SetText("Gold: " + std::to_string(resources->getGold()));
	cost->SetText("Cost: " + std::to_string(trade->getCost(Tradegoods, type, true, civNumber)));
}

void TradeGui::changeCiv(int civNr)
{
	civNumber = civNr;
	civChosen = true;
	civScrollBox->Show(false);
	civInfoBox->Show(true);
	civName->SetText(civ->getNames().at(civNr));
	civGold->SetText("Gold: " + std::to_string(civ->getcivs()->at(civNr).getGold()));
	civImage->SetImage(civ->getcivs()->at(civNr).getIcon()->GetImage());
	civInfo->SetText(civ->getcivs()->at(civNr).getInfoString());
}

void TradeGui::doTrade(sf::String input, bool add)
{
	std::string textString;
	int f = 0;
	try
	{
		f = std::stoi(input.toAnsiString());
	}
	catch (...)
	{
		printf("Invalid stoi argument\n");
		entry->SetText("");
		return;
	}
	if (civChosen)
	{
	}
}

void TradeGui::addToDesktop(sfg::Desktop * desktop)
{
	this->desktop = desktop;
	desktop->Add(window);
	desktop->Add(textWindow);
}

void TradeGui::activateTradeGui(bool active)
{
	window->Show(active);
}

void TradeGui::update()
{
	if (!Tradegoods)
	{
		yourValue->SetText("You: " + std::to_string(resources->getResource((Resources::Resource)(type))));
		theirValue->SetText("Their: " + std::to_string(civ->getcivs()->at(civNr).getResource()->getResource((Resources::Resource)(type))));
	}
	else
	{
		yourValue->SetText("You: " + std::to_string(resources->getTradeGoods((Resources::TradeGoods)(type))));
		theirValue->SetText("Their: " + std::to_string(civ->getcivs()->at(civNr).getResource()->getTradeGoods((Resources::TradeGoods)(type))));
	}
	civGold->SetText("Gold: " + std::to_string(civ->getcivs()->at(civNumber).getGold()));
	yourGold->SetText("Gold: " + std::to_string(resources->getGold()));
	cost->SetText("Cost: 300"); // add cost
}