#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include <string>

#include "Resources.h"
#include "Trade.h"
#include "CivHandler.h"
#include "EventQueue.h"
#include "GUIEvent.h"
/**
* it is as of 17/12/2017 not used. Deprecated see TradeGuiTest.
*/
class TradeGui
{
public:
	/*
	* Sets up the InformationTableWindow
	* @param a pointer to the players Resource class
	* @param pointer to civHandler
	* @param pointer the trade class
	* @param a pointer to the GUI EventQueue
	*/
	void tradeSetUp(Resources *res, CivHandler* civ, Trade* trade, EventQueue<GUIEvent>* eventQueue);
	/*
	* Adds the TradeGui to Desktop
	* @param the sfg::Desktop you want to add it too
	*/
	void addToDesktop(sfg::Desktop *desktop);
	/*
	* Shows the TradeGui based on a bool
	* @param true = show
	*/
	void activateTradeGui(bool active);
	/*
	* Updates the TradeGui
	*/
	void update();
private:
	void createResourceButton(sf::String name, sfg::Box::Ptr box, int type);
	void createTradeGoodsButton(sf::String name, sfg::Box::Ptr box, int type);
	void createCivButton(sf::String name, sfg::Box::Ptr box, int civNr);

	void changeGoods(int check, int type);
	void changeCiv(int civNr);
	void doTrade(sf::String input, bool add);

	EventQueue<GUIEvent>* eventQueue;
	sfg::Desktop * desktop;
	Resources* resources;
	Trade* trade;
	CivHandler* civ;
	int civNr;

	sfg::Window::Ptr window;

	sfg::Label::Ptr TopLabel;
	sfg::Label::Ptr yourValue;
	sfg::Label::Ptr theirValue;
	sfg::Label::Ptr yourGold;
	sfg::Label::Ptr cost;

	sfg::Entry::Ptr entry;

	sfg::Window::Ptr textWindow;
	sfg::Label::Ptr text;

	sfg::Label::Ptr civName;
	sfg::ScrolledWindow::Ptr civScrollBox;
	sfg::Box::Ptr civInfoBox;
	sfg::Label::Ptr civGold;
	sfg::Image::Ptr civImage;
	sfg::Label::Ptr civInfo;

	int type;
	int civNumber;
	bool Tradegoods;
	bool civChosen;
};
