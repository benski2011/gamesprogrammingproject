#include "TradeGuiTest.h"

void TradeGuiTest::tradeSetUp(Resources * res, CivHandler * civ, Trade * trade, PopUpGui* pop)
{
	resources = res;
	this->civ = civ;
	popUp = pop;
	this->trade = trade;
	civNr = -1;
	yourCounter = 0;
	theirCounter = 0;
	typeList = -1;

	window = sfg::Window::Create();
	window->SetAllocation(sf::FloatRect(325, 0, 1325, 900));
	window->SetRequisition(sf::Vector2f(900, 900));
	window->SetStyle(window->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	auto mainBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto civBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);

	auto civScrollBox = sfg::ScrolledWindow::Create();
	civScrollBox->SetRequisition(sf::Vector2f(0, 0));
	civScrollBox->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_NEVER | sfg::ScrolledWindow::HORIZONTAL_AUTOMATIC);
	civScrollBox->SetPlacement(sfg::ScrolledWindow::Placement::TOP_LEFT);
	civScrollBox->AddWithViewport(civBox);

	window->Add(mainBox);
	mainBox->Pack(civScrollBox, false, false);
	std::vector<std::string> civNames = civ->getNames();
	for (size_t i = 0; i < civNames.size(); i += NrOfCivPerLine)
	{
		auto tempBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

		for (size_t j = 0; j < NrOfCivPerLine; j++)
		{
			if (j + i < civNames.size())
			{
				sf::String temp = civNames[i + j];
				createCivButton(temp, tempBox, i + j);
			}
		}
		civBox->Pack(tempBox, true, true);
	}
	auto yourTradeBoxFrame = sfg::Frame::Create("Your Trade:");
	civFrame = sfg::Frame::Create("Their Trade: ");
	auto yourTrade = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	auto civTrade = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	yourTrade->Pack(yourTradeBoxFrame);
	civTrade->Pack(civFrame);

	yourTradeBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	civTradeBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);

	auto midBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	midBox->SetRequisition(sf::Vector2f(200, 0));
	auto tempButton = sfg::Button::Create("trade!");
	tempButton->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() {
		this->doTrade();
	});
	buttons.push_back(tempButton);
	midBox->Pack(tempButton, false, false);
	auto tempButton2 = sfg::Button::Create("Clear!");
	tempButton2->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() { this->clear(); });
	buttons.push_back(tempButton2);
	midBox->Pack(tempButton2, false, false);

	auto yourTradeScroll = sfg::ScrolledWindow::Create();
	yourTradeScroll->SetRequisition(sf::Vector2f(200, 400));
	yourTradeScroll->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_AUTOMATIC | sfg::ScrolledWindow::HORIZONTAL_NEVER);
	yourTradeScroll->SetPlacement(sfg::ScrolledWindow::Placement::TOP_LEFT);
	yourTradeScroll->AddWithViewport(yourTradeBox);
	yourTradeBoxFrame->Add(yourTradeScroll);

	auto civTradeScroll = sfg::ScrolledWindow::Create();
	civTradeScroll->SetRequisition(sf::Vector2f(200, 400));
	civTradeScroll->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_AUTOMATIC | sfg::ScrolledWindow::HORIZONTAL_NEVER);
	civTradeScroll->SetPlacement(sfg::ScrolledWindow::Placement::TOP_LEFT);
	civTradeScroll->AddWithViewport(civTradeBox);
	civFrame->Add(civTradeScroll);

	auto Add1 = sfg::Button::Create("Add");
	Add1->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() { this->addMenu(1); });
	Add1->SetId("Trade0");
	buttons.push_back(Add1);
	auto Add2 = sfg::Button::Create("Add");
	Add2->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() { this->addMenu(0); });
	Add2->SetId("Trade0");
	buttons.push_back(Add2);
	yourTrade->Pack(Add1, false, false);
	civTrade->Pack(Add2, false, false);

	auto tradeBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	tradeBox->Pack(yourTrade);
	tradeBox->Pack(midBox, false, false);
	tradeBox->Pack(civTrade);

	mainBox->Pack(tradeBox, false, false);

	auto buttonBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	mainBox->Pack(buttonBox, false, false);

	choseWindow = sfg::Window::Create();

	choseWindow->SetAllocation(sf::FloatRect(350, 200, 800, 300));
	choseWindow->SetRequisition(sf::Vector2f(800, 400));
	choseWindow->SetStyle(choseWindow->GetStyle() ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
	choseWindow->Show(false);

	auto choseBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
	choseWindow->Add(choseBox);

	auto resBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	auto tgBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);

	auto resFrame = sfg::Frame::Create("Resources:");
	auto tgFrame = sfg::Frame::Create("Trade Goods: ");
	resFrame->Add(resBox);

	std::vector<sf::String>* temp = resources->getResourceName();
	for (size_t i = 0; i < resources->NUM_OF_RESOURCES; i++)
	{
		createResourceButton(temp->at(i), resBox, i);
	}
	auto button = sfg::Button::Create("Gold");
	int i = 1;
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() {
		if (this->typeList == -1)
		{
			addRes->SetState(sfg::Widget::State::NORMAL);
		}
		this->typeList = 0;
		this->type = 2;
		this->updateAddMenu();
	});
	button->SetRequisition(sf::Vector2f(50, 50));
	resBox->Pack(button);

	auto tgScroll = sfg::ScrolledWindow::Create();
	tgScroll->SetRequisition(sf::Vector2f(0, 100));
	tgScroll->SetScrollbarPolicy(sfg::ScrolledWindow::VERTICAL_NEVER | sfg::ScrolledWindow::HORIZONTAL_ALWAYS);
	tgScroll->SetPlacement(sfg::ScrolledWindow::Placement::TOP_LEFT);
	tgScroll->AddWithViewport(tgBox);
	tgFrame->Add(tgScroll);

	choseBox->Pack(resFrame, false);
	choseBox->Pack(tgFrame, false, false);
	temp = resources->gettradeGoodsName();
	for (size_t i = 0; i < resources->NUM_OF_TRADEGOODS; i += NrOfTGPerLine)
	{
		auto tempBox = sfg::Box::Create(sfg::Box::Orientation::VERTICAL);
		for (size_t f = 0; f < NrOfTGPerLine; f++)
		{
			if (i + f < resources->NUM_OF_TRADEGOODS)
			{
				createTradeGoodsButton(temp->at(i + f), tempBox, i + f);
			}
		}
		tgBox->Pack(tempBox);
	}
	auto counterBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	createCountButton(-1, counterBox);
	createCountButton(-10, counterBox);
	createCountButton(-100, counterBox);
	createCountButton(1, counterBox);
	createCountButton(10, counterBox);
	createCountButton(100, counterBox);
	choseBox->Pack(counterBox);
	auto labelBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);
	typeChosen = sfg::Label::Create("Chosen: None");
	typeChosen->SetRequisition(sf::Vector2f(0, 100));
	maxAmount = sfg::Label::Create("Max amount: 0");
	maxAmount->SetRequisition(sf::Vector2f(0, 100));
	labelBox->Pack(typeChosen);
	labelBox->Pack(maxAmount);
	choseBox->Pack(labelBox);
	amount = sfg::Entry::Create("");

	choseBox->Pack(amount, false);

	addRes = sfg::Button::Create("Add");
	addRes->SetState(sfg::Widget::State::INSENSITIVE);
	addRes->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() {
		this->addStuff(); });
	auto abortButton = sfg::Button::Create("Abort");
	abortButton->GetSignal(sfg::Widget::OnLeftClick).Connect([this]() { this->choseWindow->Show(false); });

	auto addButtonBox = sfg::Box::Create(sfg::Box::Orientation::HORIZONTAL);

	addButtonBox->Pack(addRes);
	addButtonBox->Pack(abortButton);
	choseBox->Pack(addButtonBox);

	for (auto b : buttons)
	{
		b->SetState(sfg::Widget::State::INSENSITIVE);
	}
}

void TradeGuiTest::addToDesktop(sfg::Desktop * desktop)
{
	desktop->Add(window);
	desktop->Add(choseWindow);
	this->desktop = desktop;
}

void TradeGuiTest::activateTradeGui(bool active)
{
	window->Show(active);
}

void TradeGuiTest::update()
{
}

void TradeGuiTest::createCivButton(sf::String name, sfg::Box::Ptr box, int civNr)
{
	auto button = sfg::Button::Create(name);

	button->GetSignal(sfg::Widget::OnLeftClick).Connect([civNr, this]() {this->changeCiv(civNr); });
	button->SetRequisition(sf::Vector2f(50, 50));
	box->Pack(button);
}

void TradeGuiTest::createResourceButton(sf::String name, sfg::Box::Ptr box, int type)
{
	auto button = sfg::Button::Create(name);
	int i = 1;
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([this, type]() {
		if (this->typeList == -1)
		{
			addRes->SetState(sfg::Widget::State::NORMAL);
		}
		this->typeList = type;
		this->type = 0;
		this->updateAddMenu();
	});
	button->SetRequisition(sf::Vector2f(50, 50));
	box->Pack(button);
}

void TradeGuiTest::createTradeGoodsButton(sf::String name, sfg::Box::Ptr box, int type)
{
	auto button = sfg::Button::Create(name);

	button->GetSignal(sfg::Widget::OnLeftClick).Connect([this, type]() {
		if (this->typeList == -1)
		{
			addRes->SetState(sfg::Widget::State::NORMAL);
		}
		this->typeList = type;
		this->type = 1;
		this->updateAddMenu();
	});
	button->SetRequisition(sf::Vector2f(50, 50));
	box->Pack(button);
}

void TradeGuiTest::createCountButton(int number, sfg::Box::Ptr box)
{
	auto button = sfg::Button::Create(std::to_string(number));
	if (number > 0)
	{
		button->SetLabel("+" + button->GetLabel());
	}
	button->GetSignal(sfg::Widget::OnLeftClick).Connect([this, number]
	{
		int nrOfResources = 0;
		try
		{
			nrOfResources = std::stoi(amount->GetText().toAnsiString());
		}
		catch (...)
		{
		}
		nrOfResources += number;
		if (nrOfResources < 0)
		{
			nrOfResources = 0;
		}
		amount->SetText(std::to_string(nrOfResources));
	});
	box->Pack(button);
}

void TradeGuiTest::addMenu(int i)
{
	choseWindow->Show(true);
	desktop->BringToFront(choseWindow);
	//window->Show(false);
	yourList = i;

	typeChosen->SetText("Chosen: None");
	maxAmount->SetText("Max amount: 0");
}

void TradeGuiTest::addStuff()
{
	int nrOfResources = 0;
	sf::String tempString;
	int tempInt;

	try
	{
		nrOfResources = std::stoi(amount->GetText().toAnsiString());
	}
	catch (...)
	{
		//printf("Invalid stoi argument\n");
		amount->SetText("");
		return;
	}
	if (nrOfResources == 0)
	{
		return;
	}

	if (yourList)
	{
		if (type == 0)
		{
			tempInt = resources->getResource((Resources::Resource)typeList);
			tempString = resources->getResourceName()->at(typeList);
		}
		else if (type == 2)
		{
			tempInt = resources->getGold();
			tempString = "Gold";
		}
		else
		{
			tempInt = resources->getTradeGoods((Resources::TradeGoods)typeList);
			tempString = resources->gettradeGoodsName()->at(typeList);
		}
		if (nrOfResources > tempInt)
		{
			popUp->activatePopUp(true, "You can't afford it!");
			return;
		}
		auto temp = std::find_if(std::begin(your), std::end(your), [&tempString](TradeSum s) { return(tempString == s.name); });
		if (std::end(your) != temp)
		{
			if (temp->used == true)
			{
				if (nrOfResources + temp->amount > tempInt || nrOfResources + temp->amount < 0)
				{
					popUp->activatePopUp(true, "You have added to much.\n You cant afford it!");

					return;
				}
				temp->amount += nrOfResources;
				temp->label->SetText(temp->name + ": " + std::to_string(temp->amount));
				temp->label->Show(true);
			}
			else
			{
				temp->amount = nrOfResources;
				temp->used = true;
				temp->label->SetText(temp->name + ": " + std::to_string(temp->amount));
				temp->label->Show(true);
			}
		}
		else
		{
			if (nrOfResources < 0)
			{
				popUp->activatePopUp(true, "Negative number\n Try adding to the other side");
				return;
			}
			if (yourCounter >= your.size())
			{
				TradeSum temp;
				temp.box = sfg::Box::Create();
				temp.box->SetSpacing(25);
				temp.label = sfg::Label::Create("");
				temp.check = sfg::CheckButton::Create("");
				temp.box->Pack(temp.check, false, false);
				temp.box->Pack(temp.label);
				your.push_back(temp);
				yourTradeBox->Pack(temp.box);
			}
			your[yourCounter].amount = nrOfResources;
			your[yourCounter].used = true;
			your[yourCounter].name = tempString;
			your[yourCounter].type = type;
			your[yourCounter].typeList = typeList;
			your[yourCounter].label->SetText(your[yourCounter].name + ": " + std::to_string(your[yourCounter].amount));
			your[yourCounter].label->Show();
			yourCounter++;
		}
	}
	else
	{
		Resources* res = civ->getcivs()->at(civNr).getResource();
		if (type == 0)
		{
			tempInt = res->getResource((Resources::Resource)typeList);
			tempString = res->getResourceName()->at(typeList);
		}
		else if (type == 2)
		{
			tempInt = res->getGold();
			tempString = "Gold";
		}
		else
		{
			tempInt = res->getTradeGoods((Resources::TradeGoods)typeList);
			tempString = res->gettradeGoodsName()->at(typeList);
		}
		if (nrOfResources > tempInt)
		{
			popUp->activatePopUp(true, "They cant afford it!");
			return;
		}
		auto temp = std::find_if(std::begin(their), std::end(their), [tempString](TradeSum s) { return(tempString == s.name); });
		if (std::end(their) != temp)
		{
			if (temp->used == true)
			{
				if (nrOfResources + temp->amount > tempInt || nrOfResources + temp->amount < 0)
				{
					popUp->activatePopUp(true, "You have added to much.\n They cant afford it!");

					return;
				}
				temp->amount += nrOfResources;
				temp->label->SetText(temp->name + ": " + std::to_string(temp->amount));
				temp->label->Show(true);
			}
			else
			{
				temp->amount = nrOfResources;
				temp->used = true;
				temp->label->SetText(temp->name + ": " + std::to_string(temp->amount));
				temp->label->Show(true);
			}
		}
		else
		{
			if (nrOfResources < 0)
			{
				popUp->activatePopUp(true, "Negative number\n Try adding to the other side");
				return;
			}
			if (theirCounter >= their.size())
			{
				TradeSum temp;
				temp.box = sfg::Box::Create();
				temp.box->SetSpacing(25);
				temp.label = sfg::Label::Create("");
				temp.check = sfg::CheckButton::Create("");
				temp.box->Pack(temp.check, false, false);
				temp.box->Pack(temp.label);
				their.push_back(temp);
				civTradeBox->Pack(temp.box);
			}
			their[theirCounter].amount = nrOfResources;
			their[theirCounter].used = true;
			their[theirCounter].name = tempString;
			their[theirCounter].type = type;
			their[theirCounter].typeList = typeList;
			their[theirCounter].label->SetText(tempString + ": " + std::to_string(their[theirCounter].amount));
			their[theirCounter].label->Show();
			theirCounter++;
		}
	}
	amount->SetText("");
	choseWindow->Show(false);
	addRes->SetState(sfg::Widget::State::INSENSITIVE);
	typeList = -1;
}

void TradeGuiTest::changeCiv(int c)
{
	if (civNr == -1)
	{
		for (auto b : buttons)
		{
			b->SetState(sfg::Widget::State::NORMAL);
		}
	}
	sf::String temp = this->civ->getNames().at(c);

	civFrame->SetLabel(temp);
	civNr = c;
	sf::String tempString;
	int tempInt;

	for (size_t i = 0; i < their.size(); i++)
	{
		if (their.at(i).type == 0)
		{
			tempString = resources->getResourceName()->at(their.at(i).typeList);
			tempInt = resources->getResource((Resources::Resource)their.at(i).typeList);
		}
		else if (their.at(i).type == 2)
		{
			tempInt = resources->getGold();
			tempString = "Gold";
		}
		else
		{
			tempString = resources->gettradeGoodsName()->at(their.at(i).typeList);
			tempInt = resources->getTradeGoods((Resources::TradeGoods)their.at(i).typeList);
		}

		if (tempInt < their.at(i).amount)
		{
			their.at(i).amount = tempInt;
		}
	}
}

void TradeGuiTest::clear()
{
	for (size_t i = 0; i < your.size(); i++)
	{
		your.at(i).used = false;
		your.at(i).label->Show(false);
	}
	yourCounter = 0;
	yourTradeBox->RemoveAll();
	your.clear();
	for (size_t i = 0; i < their.size(); i++)
	{
		their.at(i).used = false;
		their.at(i).label->Show(false);
	}
	civTradeBox->RemoveAll();
	their.clear();
	theirCounter = 0;
}

void TradeGuiTest::updateAddMenu()
{
	if (yourList)
	{
		sf::String tempString;
		int tempInt;
		if (type == 0)
		{
			tempString = resources->getResourceName()->at(typeList);
			tempInt = resources->getResource((Resources::Resource)typeList);
		}
		else if (type == 2)
		{
			tempInt = resources->getGold();
			tempString = "Gold";
		}
		else
		{
			tempString = resources->gettradeGoodsName()->at(typeList);
			tempInt = resources->getTradeGoods((Resources::TradeGoods)typeList);
		}

		typeChosen->SetText("Chosen: " + tempString);
		maxAmount->SetText("Max amount: " + std::to_string(tempInt));
	}
	else
	{
		Resources* res = civ->getcivs()->at(civNr).getResource();
		sf::String tempString;
		int tempInt;
		if (type == 0)
		{
			tempString = res->getResourceName()->at(typeList);
			tempInt = res->getResource((Resources::Resource)typeList);
		}
		else if (type == 2)
		{
			tempInt = res->getGold();
			tempString = "Gold";
		}
		else
		{
			tempString = res->gettradeGoodsName()->at(typeList);
			tempInt = res->getTradeGoods((Resources::TradeGoods)typeList);
		}

		typeChosen->SetText("Chosen: " + tempString);
		maxAmount->SetText("Max amount: " + std::to_string(tempInt));
	}
}

void TradeGuiTest::doTrade()
{
	if (trade->doTrade(your, their, civNr))
	{
		popUp->activatePopUp(true, "Trade Accepted!");
		clear();
	}
	else
	{
		popUp->activatePopUp(true, "Trade not Accepted!!");
	}
}