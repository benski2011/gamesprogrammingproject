#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>
#include <string>
#include <algorithm>

#include "Resources.h"
#include "Trade.h"
#include "CivHandler.h"
#include "EventQueue.h"
#include "GUIEvent.h"
#include "PopUpGui.h"
#define NrOfCivPerLine 3
#define NrOfTGPerLine 3

class Trade;

class TradeGuiTest
{
public:
	/*
	* Sets up the InformationTableWindow
	* @param a pointer to the players Resource class
	* @param pointer to civHandler
	* @param pointer the trade class
	* @param a pointer to the GUI EventQueue
	*/
	void tradeSetUp(Resources *res, CivHandler* civ, Trade* trade, PopUpGui* pop);
	/*
	* Adds the TradeGui to Desktop
	* @param the sfg::Desktop you want to add it too
	*/
	void addToDesktop(sfg::Desktop *desktop);
	/*
	* Shows the TradeGui based on a bool
	* @param true = show
	*/
	void activateTradeGui(bool active);
	/*
	* Updates the TradeGui
	*/
	void update();
private:
	void createCivButton(sf::String name, sfg::Box::Ptr box, int civNr);
	void createResourceButton(sf::String name, sfg::Box::Ptr box, int type);
	void createTradeGoodsButton(sf::String name, sfg::Box::Ptr box, int type);
	void createCountButton(int number, sfg::Box::Ptr box);
	void addMenu(int i);
	void addStuff();
	void changeCiv(int civ);
	void clear();
	void updateAddMenu();
	void doTrade();

	sfg::Window::Ptr window;
	sfg::Window::Ptr choseWindow;
	sfg::Box::Ptr yourTradeBox;
	sfg::Box::Ptr civTradeBox;
	sfg::Label::Ptr typeChosen;
	sfg::Label::Ptr maxAmount;
	sfg::Entry::Ptr amount;
	sfg::Frame::Ptr civFrame;
	sfg::Button::Ptr addRes;

	Resources* resources;
	Trade* trade;
	CivHandler* civ;
	sfg::Desktop* desktop;
	PopUpGui* popUp;
	int civNr;
	bool yourList;
	int type;
	int typeList;

	std::vector<sfg::Button::Ptr> buttons;

	std::vector<TradeSum> your;
	std::vector<TradeSum> their;

	int yourCounter;
	int theirCounter;
};
