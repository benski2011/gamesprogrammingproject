#include "WorldMap.h"

WorldMap::WorldMap()
{
	std::random_device rd;
	randomGenerator = std::mt19937_64(rd());
	std::uniform_int_distribution<int> distributorHomeTown(6, 55);

	homeTown.x = distributorHomeTown(randomGenerator);
	homeTown.y = distributorHomeTown(randomGenerator);

	int values[4] = { -200 };

	std::vector<std::vector<int>> temp = diamondSquare::diamondSquare(7, 100, values);

	for (size_t i = 0; i < WORLDSIZE; i++)
	{
		for (size_t f = 0; f < WORLDSIZE; f++)
		{
			tiles[i][f].tagged = false;
			tiles[i][f].type = UNKNOWN;
			if (temp[i][f] < -10)
			{
				tiles[i][f].terrain = WATER;
			}
			else if (temp[i][f] > 50)
			{
				tiles[i][f].terrain = MOUNTAIN;
			}
			else
			{
				tiles[i][f].terrain = GROUND;
			}
		}
	}
	tiles[homeTown.x][homeTown.y].type = HOMECITY;
	for (int i = -1; i < 2; ++i)
	{
		for (int j = -1; j < 2; ++j)
		{
			if (i == 0 && j == 0)
			{
				continue;
			}
			else
			{
				discoverTile(sf::Vector2i(i + homeTown.x, j + homeTown.y));
			}
		}
	}
}

WorldMap::~WorldMap()
{
}

void WorldMap::update(GUIEvent event)
{
	switch (event.type)
	{
	case GUIEvent::EXPLORE:
		explore();

		break;
	default:
		break;
	}
}

void WorldMap::draw(sf::RenderWindow &window)
{
	sf::RectangleShape rect;
	rect.setFillColor(sf::Color(0, 0, 0, 0));
	rect.setOutlineColor(sf::Color(255, 255, 255, 255));
	rect.setOutlineThickness(1.0f);
	//rect.setSize(glm::vec2(RECTSIZE, RECTSIZE));
	rect.setOrigin(rect.getSize() / 2.0f);

	for (size_t x = 0; x < WORLDSIZE; x++)
	{
		for (size_t y = 0; y < WORLDSIZE; y++)

		{
			bool draw = false;
			switch (tiles[x][y].terrain)
			{
			case GROUND:
				if (tiles[x][y].type == UNKNOWN)
				{
					rect.setFillColor(sf::Color(53, 105, 40));
				}
				else
				{
					rect.setFillColor(sf::Color(89, 180, 64));
				}
				break;
			case MOUNTAIN:
				if (tiles[x][y].type == UNKNOWN)
				{
					rect.setFillColor(sf::Color(17, 16, 14));
				}
				else
				{
					rect.setFillColor(sf::Color(92, 75, 54));
				}
				break;
			case WATER:
				if (tiles[x][y].type == UNKNOWN)
				{
					rect.setFillColor(sf::Color(15, 41, 75));
				}
				else
				{
					rect.setFillColor(sf::Color(33, 96, 176));
				}
			default:
				break;
			}
			if (tiles[x][y].tagged)
			{
				rect.setOutlineColor(sf::Color(223, 187, 41));
			}
			else
			{
				rect.setOutlineColor(sf::Color(255, 255, 255, 255));
			}
			//rect.setPosition(glm::vec2(x * RECTSIZE + 400, y * RECTSIZE));
			window.draw(rect);

			/*	switch (tiles[x][y].type)
				{
				case BUILDABLE:
					rect.setFillColor(sf::Color::Green);
					break;
				case NORESOURCE:
					rect.setFillColor(sf::Color::Black);
					break;
				case CIVILISATION:
					rect.setFillColor(sf::Color::Red);
					break;
				case HOMECITY:
					rect.setFillColor(sf::Color::Cyan);
					break;
				case UNKNOWN:
					if (tiles[x][y].tagged)
					{
						draw = true;
					}
					rect.setFillColor(sf::Color::Blue);
					break;
				default:
					rect.setFillColor(sf::Color::Magenta);
					break;
				}
				*/
		}
	}
}

void WorldMap::discoverTile(sf::Vector2i pos)
{
	const int ChanceOfBuildable = 40;

	std::uniform_int_distribution<int> distributorResources(0, Resources::Resource::NUM_OF_RESOURCES - 1);

	std::uniform_int_distribution<int> distributorBuildable(0, 100);
	if (distributorBuildable(randomGenerator) <= ChanceOfBuildable)
	{
		tiles[pos.x][pos.y].type = BUILDABLE;

		tiles[pos.x][pos.y].resource = (Resources::Resource)(distributorResources(randomGenerator));
	}
	else
	{
		tiles[pos.x][pos.y].type = NORESOURCE;
	}
}

void WorldMap::explore()
{
	for (size_t i = 0; i < WORLDSIZE; i++)
	{
		for (size_t f = 0; f < WORLDSIZE; f++)

		{
			if (tiles[i][f].type != UNKNOWN)
			{
				for (int k = -1; k < 2; ++k)
				{
					for (int l = -1; l < 2; ++l)
					{
						if (abs(k) == abs(l))
						{
							continue;
						}

						if (tiles[i + k][f + l].type == UNKNOWN)

						{
							tiles[i + k][f + l].tagged = true;
						}
					}
				}
			}
		}
	}
}