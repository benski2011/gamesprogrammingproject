#pragma once
#include "SFML\Graphics.hpp"
#include <random>

#include "Map.h"
#include "GUIEvent.h"
#include "diamondSquare.h"

const int WORLDSIZE = 65;
const int RECTSIZE = 20;

enum WorldType
{
	BUILDABLE,
	NORESOURCE,
	CIVILISATION,
	HOMECITY,
	UNKNOWN,
};

enum Worldterrain
{
	GROUND,
	WATER,
	MOUNTAIN
};

class WorldMap
{
public:

	struct WorldTile
	{
		WorldType type;
		Worldterrain terrain;

		Resources::Resource resource;

		bool tagged;
	};

	WorldMap();
	~WorldMap();

	void update(GUIEvent event);

	void draw(sf::RenderWindow &window);

	void discoverTile(sf::Vector2i pos);

private:
	void explore();

	WorldTile tiles[WORLDSIZE][WORLDSIZE];

	sf::Vector2i homeTown;

	std::mt19937_64 randomGenerator;
};
