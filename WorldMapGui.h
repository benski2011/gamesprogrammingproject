#pragma once
#include <SFGUI\SFGUI.hpp>
#include <SFGUI\Widgets.hpp>

#include "EventQueue.h"
#include "GUIEvent.h"

class WorldMapGui
{
public:
	/*
	* Sets up the WorldMapGui
	* @param a pointer to the GUI EventQueue
	*/
	void worldMapSetUp(EventQueue<GUIEvent>* eventQueue);
	/*
	* Sets the visability of the WorldMapGui
	* @param true = show
	*/
	void activateWorldMapGui(bool active);
	/*
	* Adds the WorldMapGui to a sfg::Box
	* @param the Box you want to attach it too
	*/
	void AddToBox(sfg::Box::Ptr box);
private:

	sfg::Table::Ptr table;
};
