#include "rainfall.h"

const int seaLevel = 50;

std::vector<std::vector<bool>> mapGeneration::generateWater(std::vector<std::vector<int>>& heightMap)
{
	std::vector<std::vector<bool>> map;

	map.resize(heightMap.size());
	for (auto i = 0; i < heightMap.size(); ++i)
	{
		map[i].resize(heightMap[i].size());
		for (auto j = 0; j < heightMap[i].size(); ++j)
		{
			map[i][j] = false;
			if (heightMap[i][j] <= seaLevel)
			{
				map[i][j] = true;
				heightMap[i][j] = seaLevel;
			}
		}
	}

	return map;
}