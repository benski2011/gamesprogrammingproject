#include <vector>
#include <random>
#include <algorithm>
#include <SFML\Graphics\Vertex.hpp>

namespace mapGeneration
{
	struct Lake
	{
		std::vector<sf::Vector2i> tiles;
	};
	/**
	*	Generate water/rainfall
	*	@param the heightmap
	*	@return the watermap
	*/
	std::vector<std::vector<bool>> generateWater(std::vector<std::vector<int>>& heightMap);
}