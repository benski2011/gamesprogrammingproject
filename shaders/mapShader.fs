#version 330 core

out vec4 color;
in vec3 colors;

in float height;
flat in int tile;

uniform int tileToDraw;
uniform int allResources;

uniform sampler2D texture0;
uniform usampler2D texture1;

float rand(vec2 n) { 
	return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float noise(vec2 p, float det)
{
	vec2 ip = floor(p);
	vec2 u = fract(p);
	u = u*u*(3.0-2.0*u);
	
	float res = mix(
		mix(rand(ip),rand(ip+vec2(1.0,0.0)),u.x),
		mix(rand(ip+vec2(0.0,1.0)),rand(ip+vec2(1.0,1.0)),u.x),u.y);
	res = floor(res * res * 2.0 * det);
	return res;
}

float DistToLine(vec2 pt1, vec2 pt2, vec2 testPt)
{
  vec2 lineDir = pt2 - pt1;
  vec2 perpDir = vec2(lineDir.y, -lineDir.x);
  vec2 dirToPt1 = pt1 - testPt;
  return abs(dot(normalize(perpDir), dirToPt1));
}

vec2 getTextCoordForTile(int x, int y)
{
	uint sample = texture(texture1, vec2(float(x) / 512.0, (float(y) / 512.0))).r;
	
	return vec2(((sample % 4u) * 128u + colors.y * 127.0) / 512.0, 1.0 - (((sample / 4u) * 128u + colors.x * 127.0) / 512.0));
}

void drawTextured()
{
	int y = tile % 512;
	int x = tile / 512;
	
	vec2 left = getTextCoordForTile(max(x - 1, 0), y);
	vec2 right = getTextCoordForTile(min(x + 1, 511), y);
	vec2 up = getTextCoordForTile(x, min(y + 1, 511));
	vec2 down = getTextCoordForTile(x, max(y - 1, 0));
	
	color = vec4(texture(texture0, getTextCoordForTile(x, y)).xyz, 1.0);
	
	vec2 normTextCoords = vec2((colors.x < 0.5) ? colors.x : 1.0 - colors.x, (colors.y < 0.5) ? colors.y : 1.0 - colors.y);
	
	float det = DistToLine(vec2(0.0, 0.0), vec2(0.5, 0.5), normTextCoords.xy);
}

void drawResource()
{
	color = (tile == tileToDraw || tileToDraw == allResources) ? vec4(0.0, 0.5, 0.0, 1.0) : vec4(0.5, 0.0, 0.0, 1.0);
	if(colors.x < 0.02 || colors.x > 0.98 || colors.y < 0.02 || colors.y > 0.98)
	{
		color = vec4(0.0, 0.0, 0.0, 1.0);
	}
}

void main()
{
	(tileToDraw == -1) ? drawTextured() : drawResource();
}