#version 330 core

out float height;

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec3 color;
layout(location = 2) in int tileResource;

uniform mat4 view;
uniform mat4 projection;

float size = 16.0;

out vec3 colors;
flat out int tile;

void main()
{
	tile = gl_VertexID / 4;
	gl_Position = projection * view * vec4(vertexPos, 1.0);
	
	colors = color;
	tile = tileResource;
}