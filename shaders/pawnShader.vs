#version 330 core

layout(location = 0) in vec3 vertexPos;
layout(location = 1) in vec2 textCoord;
layout(location = 2) in vec3 diffuse;
layout(location = 3) in vec3 specular;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 diffuseColor;
out vec3 specularColor;
out vec2 textureCoordinates;

void main()
{
	gl_Position = projection * view * model * vec4(vertexPos, 1.0);
	
	diffuseColor = diffuse;
	specularColor = specular;
	textureCoordinates = textCoord;
}