#include "terrainGenerator.h"

std::vector<std::vector<terrain>> terrainGenerator::generateMap(int size, std::vector<std::vector<int>> heightMap, std::vector<std::vector<bool>> waterMap)
{
	std::random_device rd;
	std::mt19937 randomGen(rd());
	std::uniform_int_distribution<int> distributor(0, 500);

	std::vector<std::vector<terrain>> map;
	std::vector<std::vector<int>> heatMap, wetMap;

	int heatStart = distributor(randomGen);
	int wetStart = distributor(randomGen);

	int values[4] = { -600 };
	heatMap = diamondSquare::diamondSquare(size, 500, values);
	wetMap = diamondSquare::diamondSquare(size, 500, values);

	for (size_t x = 0; x < pow(2, size); x++)
	{
		std::vector<terrain> tempV;

		for (size_t y = 0; y < pow(2, size); y++)
		{
			heatMap[x][y] += heatStart;
			wetMap[x][y] += wetStart;
			heatMap[x][y] = heatMap[x][y] * (100 - (heightMap[x][y] / 100.f));
			if (heightMap[x][y] < -50)
			{
				tempV.push_back(SEA_WATER);
				/*
				for (size_t i = 0; i < 2; i++)
				{
					wetMap[x - i][y] += 150 / (i + 1);
					wetMap[x - i][y - i] += 150 / (i + 1);
					wetMap[x - i][y + i] += 150 / (i + 1);
					wetMap[x][y] += 150 / (i + 1);
					wetMap[x][y - i] += 150 / (i + 1);
					wetMap[x][y + i] += 150 / (i + 1);
					wetMap[x + i][y] += 150 / (i + 1);
					wetMap[x + i][y + i] += 150 / (i + 1);
					wetMap[x + i][y - i] += 150 / (i + 1);
				}
				*/	// FIND BETTER SOLUTION. FIRST OF ALL, BOUNDARY CHECK, SECOND OF ALL, MAYBE DO A COMPLETE DISTANCE ALGORITHM FOR THE WHOLE BOARD
			}
			else
			{
				/*if (heatMap[x][y] < 350)
				{
					if (wetMap[x][y] < 350)
					{
						tempV.push_back(POLAR_DESERT);
					}
					else if (wetMap[x][y] > 650)
					{
						tempV.push_back(ARCTIC_TUNDRA);
					}
					else
					{
						tempV.push_back(TUNDRA);
					}
				}
				else if (heatMap[x][y] > 650)
				{
					if (wetMap[x][y] < 350)
					{
						tempV.push_back(TEMPERATE_DESERT);
					}
					else if (wetMap[x][y] > 650)
					{
						tempV.push_back(WETLANDS);
					}
					else
					{
						tempV.push_back(GRASSLANDS);
					}
				}
				else
				{
					if (wetMap[x][y] < 350)
					{
						tempV.push_back(DESERT);
					}
					else if (wetMap[x][y] > 650)
					{
						tempV.push_back(TROPICAL_WETLANDS);
					}
					else
					{
						tempV.push_back(SAVANNAH);
					}
				}*/

				tempV.push_back(GRASSLANDS);
			}
		}
		map.push_back(tempV);
	}
	return map;
}