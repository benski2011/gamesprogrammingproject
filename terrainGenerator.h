#pragma once
#include <vector>
#include <random>

#include "PerlinNoise.h"
#include "Map.h"
#include "diamondSquare.h"

enum terrain;

namespace terrainGenerator
{
	/**
	* Generates a terrain map
	*
	* @param size Size of the terrain map
	* @param heightMap Heightmap of the terrain
	* @param waterMap Watermap of the terrain
	*/
	std::vector<std::vector<terrain>> generateMap(int size, std::vector<std::vector<int>> heightMap, std::vector<std::vector<bool>> waterMap);
};
